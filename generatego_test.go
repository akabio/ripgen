package ripgen

import (
	"testing"

	"github.com/akabio/expect"
)

var mkImportTests = map[string]string{
	"time.Time":               "time",
	"int":                     "",
	"string":                  "",
	"github.com/foo/bar.Date": "github.com/foo/bar",
}

func TestMkImport(t *testing.T) {
	for in, exp := range mkImportTests {
		t.Run(in, func(t *testing.T) {
			res := mkImport(in)
			expect.Value(t, "import", res).ToBe(exp)
		})
	}
}

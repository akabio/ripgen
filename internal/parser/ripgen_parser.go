// Code generated from internal/parser/Ripgen.g4 by ANTLR 4.13.0. DO NOT EDIT.

package parser // Ripgen

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type RipgenParser struct {
	*antlr.BaseParser
}

var RipgenParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func ripgenParserInit() {
	staticData := &RipgenParserStaticData
	staticData.LiteralNames = []string{
		"", "'int'", "'float'", "'string'", "'bool'", "'enum'", "'any'", "'type'",
		"'query'", "'mutation'", "'stream'", "'/'", "'+'", "'-'", "'.'", "'('",
		"')'", "','", "'external'", "'['", "']'", "'{'", "'}'", "'...'", "':'",
		"'*'", "'?'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "COL", "STAR", "NULLABLE", "COMMENT", "TYPE_IDENTIFIER",
		"PROP_IDENTIFIER", "STRING_LITERAL", "WHITESPACE",
	}
	staticData.RuleNames = []string{
		"start", "typeIdentifier", "propIdentifier", "identifier", "mimeType",
		"root", "mutation", "query", "stream", "namedType", "typeDeclaration",
		"parameters", "keyValue", "subTypeSlot", "topTypeSlot", "subTypeDef",
		"topTypeDef", "intType", "floatType", "boolType", "externalType", "stringType",
		"anyType", "arrayType", "mapType", "objectType", "objectElement", "enumType",
		"enumValue", "customType",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 31, 244, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7,
		4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2, 10, 7,
		10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15, 7, 15,
		2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7, 20, 2,
		21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25, 2, 26,
		7, 26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 1, 0, 1, 0, 1, 0, 1, 1,
		1, 1, 1, 2, 1, 2, 1, 3, 1, 3, 3, 3, 70, 8, 3, 1, 4, 1, 4, 1, 4, 1, 4, 1,
		4, 5, 4, 77, 8, 4, 10, 4, 12, 4, 80, 9, 4, 1, 5, 1, 5, 1, 5, 1, 5, 5, 5,
		86, 8, 5, 10, 5, 12, 5, 89, 9, 5, 1, 6, 5, 6, 92, 8, 6, 10, 6, 12, 6, 95,
		9, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 6, 101, 8, 6, 1, 6, 1, 6, 1, 6, 3, 6,
		106, 8, 6, 1, 7, 5, 7, 109, 8, 7, 10, 7, 12, 7, 112, 9, 7, 1, 7, 1, 7,
		1, 7, 1, 7, 3, 7, 118, 8, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 8, 5, 8, 125, 8,
		8, 10, 8, 12, 8, 128, 9, 8, 1, 8, 1, 8, 1, 8, 1, 8, 3, 8, 134, 8, 8, 1,
		8, 1, 8, 1, 9, 1, 9, 1, 9, 3, 9, 141, 8, 9, 1, 10, 5, 10, 144, 8, 10, 10,
		10, 12, 10, 147, 9, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 11, 1, 11, 1, 11,
		5, 11, 156, 8, 11, 10, 11, 12, 11, 159, 9, 11, 1, 11, 1, 11, 3, 11, 163,
		8, 11, 1, 12, 1, 12, 1, 12, 1, 13, 1, 13, 3, 13, 170, 8, 13, 1, 14, 1,
		14, 3, 14, 174, 8, 14, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15, 1, 15,
		1, 15, 3, 15, 184, 8, 15, 1, 16, 1, 16, 1, 16, 1, 16, 3, 16, 190, 8, 16,
		1, 17, 1, 17, 1, 18, 1, 18, 1, 19, 1, 19, 1, 20, 1, 20, 1, 21, 1, 21, 1,
		22, 1, 22, 1, 23, 1, 23, 1, 23, 1, 23, 1, 24, 1, 24, 1, 24, 1, 24, 1, 25,
		1, 25, 5, 25, 214, 8, 25, 10, 25, 12, 25, 217, 9, 25, 1, 25, 1, 25, 1,
		26, 1, 26, 1, 26, 3, 26, 224, 8, 26, 1, 27, 1, 27, 1, 27, 1, 27, 1, 27,
		5, 27, 231, 8, 27, 10, 27, 12, 27, 234, 9, 27, 1, 27, 1, 27, 1, 28, 1,
		28, 3, 28, 240, 8, 28, 1, 29, 1, 29, 1, 29, 0, 0, 30, 0, 2, 4, 6, 8, 10,
		12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46,
		48, 50, 52, 54, 56, 58, 0, 2, 2, 0, 1, 10, 29, 29, 1, 0, 12, 14, 246, 0,
		60, 1, 0, 0, 0, 2, 63, 1, 0, 0, 0, 4, 65, 1, 0, 0, 0, 6, 69, 1, 0, 0, 0,
		8, 71, 1, 0, 0, 0, 10, 87, 1, 0, 0, 0, 12, 93, 1, 0, 0, 0, 14, 110, 1,
		0, 0, 0, 16, 126, 1, 0, 0, 0, 18, 137, 1, 0, 0, 0, 20, 145, 1, 0, 0, 0,
		22, 157, 1, 0, 0, 0, 24, 164, 1, 0, 0, 0, 26, 167, 1, 0, 0, 0, 28, 171,
		1, 0, 0, 0, 30, 183, 1, 0, 0, 0, 32, 189, 1, 0, 0, 0, 34, 191, 1, 0, 0,
		0, 36, 193, 1, 0, 0, 0, 38, 195, 1, 0, 0, 0, 40, 197, 1, 0, 0, 0, 42, 199,
		1, 0, 0, 0, 44, 201, 1, 0, 0, 0, 46, 203, 1, 0, 0, 0, 48, 207, 1, 0, 0,
		0, 50, 211, 1, 0, 0, 0, 52, 223, 1, 0, 0, 0, 54, 225, 1, 0, 0, 0, 56, 237,
		1, 0, 0, 0, 58, 241, 1, 0, 0, 0, 60, 61, 3, 10, 5, 0, 61, 62, 5, 0, 0,
		1, 62, 1, 1, 0, 0, 0, 63, 64, 5, 28, 0, 0, 64, 3, 1, 0, 0, 0, 65, 66, 7,
		0, 0, 0, 66, 5, 1, 0, 0, 0, 67, 70, 3, 2, 1, 0, 68, 70, 3, 4, 2, 0, 69,
		67, 1, 0, 0, 0, 69, 68, 1, 0, 0, 0, 70, 7, 1, 0, 0, 0, 71, 72, 3, 6, 3,
		0, 72, 73, 5, 11, 0, 0, 73, 78, 3, 6, 3, 0, 74, 75, 7, 1, 0, 0, 75, 77,
		3, 6, 3, 0, 76, 74, 1, 0, 0, 0, 77, 80, 1, 0, 0, 0, 78, 76, 1, 0, 0, 0,
		78, 79, 1, 0, 0, 0, 79, 9, 1, 0, 0, 0, 80, 78, 1, 0, 0, 0, 81, 86, 3, 20,
		10, 0, 82, 86, 3, 12, 6, 0, 83, 86, 3, 14, 7, 0, 84, 86, 3, 16, 8, 0, 85,
		81, 1, 0, 0, 0, 85, 82, 1, 0, 0, 0, 85, 83, 1, 0, 0, 0, 85, 84, 1, 0, 0,
		0, 86, 89, 1, 0, 0, 0, 87, 85, 1, 0, 0, 0, 87, 88, 1, 0, 0, 0, 88, 11,
		1, 0, 0, 0, 89, 87, 1, 0, 0, 0, 90, 92, 5, 27, 0, 0, 91, 90, 1, 0, 0, 0,
		92, 95, 1, 0, 0, 0, 93, 91, 1, 0, 0, 0, 93, 94, 1, 0, 0, 0, 94, 96, 1,
		0, 0, 0, 95, 93, 1, 0, 0, 0, 96, 97, 5, 9, 0, 0, 97, 98, 3, 4, 2, 0, 98,
		100, 5, 15, 0, 0, 99, 101, 3, 22, 11, 0, 100, 99, 1, 0, 0, 0, 100, 101,
		1, 0, 0, 0, 101, 102, 1, 0, 0, 0, 102, 105, 5, 16, 0, 0, 103, 104, 5, 24,
		0, 0, 104, 106, 3, 28, 14, 0, 105, 103, 1, 0, 0, 0, 105, 106, 1, 0, 0,
		0, 106, 13, 1, 0, 0, 0, 107, 109, 5, 27, 0, 0, 108, 107, 1, 0, 0, 0, 109,
		112, 1, 0, 0, 0, 110, 108, 1, 0, 0, 0, 110, 111, 1, 0, 0, 0, 111, 113,
		1, 0, 0, 0, 112, 110, 1, 0, 0, 0, 113, 114, 5, 8, 0, 0, 114, 115, 3, 4,
		2, 0, 115, 117, 5, 15, 0, 0, 116, 118, 3, 22, 11, 0, 117, 116, 1, 0, 0,
		0, 117, 118, 1, 0, 0, 0, 118, 119, 1, 0, 0, 0, 119, 120, 5, 16, 0, 0, 120,
		121, 5, 24, 0, 0, 121, 122, 3, 28, 14, 0, 122, 15, 1, 0, 0, 0, 123, 125,
		5, 27, 0, 0, 124, 123, 1, 0, 0, 0, 125, 128, 1, 0, 0, 0, 126, 124, 1, 0,
		0, 0, 126, 127, 1, 0, 0, 0, 127, 129, 1, 0, 0, 0, 128, 126, 1, 0, 0, 0,
		129, 130, 5, 10, 0, 0, 130, 131, 3, 4, 2, 0, 131, 133, 5, 15, 0, 0, 132,
		134, 3, 22, 11, 0, 133, 132, 1, 0, 0, 0, 133, 134, 1, 0, 0, 0, 134, 135,
		1, 0, 0, 0, 135, 136, 5, 16, 0, 0, 136, 17, 1, 0, 0, 0, 137, 138, 3, 4,
		2, 0, 138, 140, 3, 26, 13, 0, 139, 141, 5, 27, 0, 0, 140, 139, 1, 0, 0,
		0, 140, 141, 1, 0, 0, 0, 141, 19, 1, 0, 0, 0, 142, 144, 5, 27, 0, 0, 143,
		142, 1, 0, 0, 0, 144, 147, 1, 0, 0, 0, 145, 143, 1, 0, 0, 0, 145, 146,
		1, 0, 0, 0, 146, 148, 1, 0, 0, 0, 147, 145, 1, 0, 0, 0, 148, 149, 5, 7,
		0, 0, 149, 150, 3, 2, 1, 0, 150, 151, 3, 32, 16, 0, 151, 21, 1, 0, 0, 0,
		152, 153, 3, 24, 12, 0, 153, 154, 5, 17, 0, 0, 154, 156, 1, 0, 0, 0, 155,
		152, 1, 0, 0, 0, 156, 159, 1, 0, 0, 0, 157, 155, 1, 0, 0, 0, 157, 158,
		1, 0, 0, 0, 158, 160, 1, 0, 0, 0, 159, 157, 1, 0, 0, 0, 160, 162, 3, 24,
		12, 0, 161, 163, 5, 17, 0, 0, 162, 161, 1, 0, 0, 0, 162, 163, 1, 0, 0,
		0, 163, 23, 1, 0, 0, 0, 164, 165, 3, 4, 2, 0, 165, 166, 3, 26, 13, 0, 166,
		25, 1, 0, 0, 0, 167, 169, 3, 30, 15, 0, 168, 170, 5, 26, 0, 0, 169, 168,
		1, 0, 0, 0, 169, 170, 1, 0, 0, 0, 170, 27, 1, 0, 0, 0, 171, 173, 3, 32,
		16, 0, 172, 174, 5, 26, 0, 0, 173, 172, 1, 0, 0, 0, 173, 174, 1, 0, 0,
		0, 174, 29, 1, 0, 0, 0, 175, 184, 3, 34, 17, 0, 176, 184, 3, 36, 18, 0,
		177, 184, 3, 42, 21, 0, 178, 184, 3, 46, 23, 0, 179, 184, 3, 48, 24, 0,
		180, 184, 3, 38, 19, 0, 181, 184, 3, 44, 22, 0, 182, 184, 3, 58, 29, 0,
		183, 175, 1, 0, 0, 0, 183, 176, 1, 0, 0, 0, 183, 177, 1, 0, 0, 0, 183,
		178, 1, 0, 0, 0, 183, 179, 1, 0, 0, 0, 183, 180, 1, 0, 0, 0, 183, 181,
		1, 0, 0, 0, 183, 182, 1, 0, 0, 0, 184, 31, 1, 0, 0, 0, 185, 190, 3, 50,
		25, 0, 186, 190, 3, 54, 27, 0, 187, 190, 3, 40, 20, 0, 188, 190, 3, 30,
		15, 0, 189, 185, 1, 0, 0, 0, 189, 186, 1, 0, 0, 0, 189, 187, 1, 0, 0, 0,
		189, 188, 1, 0, 0, 0, 190, 33, 1, 0, 0, 0, 191, 192, 5, 1, 0, 0, 192, 35,
		1, 0, 0, 0, 193, 194, 5, 2, 0, 0, 194, 37, 1, 0, 0, 0, 195, 196, 5, 4,
		0, 0, 196, 39, 1, 0, 0, 0, 197, 198, 5, 18, 0, 0, 198, 41, 1, 0, 0, 0,
		199, 200, 5, 3, 0, 0, 200, 43, 1, 0, 0, 0, 201, 202, 5, 6, 0, 0, 202, 45,
		1, 0, 0, 0, 203, 204, 5, 19, 0, 0, 204, 205, 3, 26, 13, 0, 205, 206, 5,
		20, 0, 0, 206, 47, 1, 0, 0, 0, 207, 208, 5, 21, 0, 0, 208, 209, 3, 26,
		13, 0, 209, 210, 5, 22, 0, 0, 210, 49, 1, 0, 0, 0, 211, 215, 5, 21, 0,
		0, 212, 214, 3, 52, 26, 0, 213, 212, 1, 0, 0, 0, 214, 217, 1, 0, 0, 0,
		215, 213, 1, 0, 0, 0, 215, 216, 1, 0, 0, 0, 216, 218, 1, 0, 0, 0, 217,
		215, 1, 0, 0, 0, 218, 219, 5, 22, 0, 0, 219, 51, 1, 0, 0, 0, 220, 224,
		3, 18, 9, 0, 221, 222, 5, 23, 0, 0, 222, 224, 3, 2, 1, 0, 223, 220, 1,
		0, 0, 0, 223, 221, 1, 0, 0, 0, 224, 53, 1, 0, 0, 0, 225, 226, 5, 5, 0,
		0, 226, 227, 5, 15, 0, 0, 227, 232, 3, 56, 28, 0, 228, 229, 5, 17, 0, 0,
		229, 231, 3, 56, 28, 0, 230, 228, 1, 0, 0, 0, 231, 234, 1, 0, 0, 0, 232,
		230, 1, 0, 0, 0, 232, 233, 1, 0, 0, 0, 233, 235, 1, 0, 0, 0, 234, 232,
		1, 0, 0, 0, 235, 236, 5, 16, 0, 0, 236, 55, 1, 0, 0, 0, 237, 239, 3, 4,
		2, 0, 238, 240, 5, 30, 0, 0, 239, 238, 1, 0, 0, 0, 239, 240, 1, 0, 0, 0,
		240, 57, 1, 0, 0, 0, 241, 242, 3, 2, 1, 0, 242, 59, 1, 0, 0, 0, 23, 69,
		78, 85, 87, 93, 100, 105, 110, 117, 126, 133, 140, 145, 157, 162, 169,
		173, 183, 189, 215, 223, 232, 239,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// RipgenParserInit initializes any static state used to implement RipgenParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewRipgenParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func RipgenParserInit() {
	staticData := &RipgenParserStaticData
	staticData.once.Do(ripgenParserInit)
}

// NewRipgenParser produces a new parser instance for the optional input antlr.TokenStream.
func NewRipgenParser(input antlr.TokenStream) *RipgenParser {
	RipgenParserInit()
	this := new(RipgenParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &RipgenParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "Ripgen.g4"

	return this
}

// RipgenParser tokens.
const (
	RipgenParserEOF             = antlr.TokenEOF
	RipgenParserT__0            = 1
	RipgenParserT__1            = 2
	RipgenParserT__2            = 3
	RipgenParserT__3            = 4
	RipgenParserT__4            = 5
	RipgenParserT__5            = 6
	RipgenParserT__6            = 7
	RipgenParserT__7            = 8
	RipgenParserT__8            = 9
	RipgenParserT__9            = 10
	RipgenParserT__10           = 11
	RipgenParserT__11           = 12
	RipgenParserT__12           = 13
	RipgenParserT__13           = 14
	RipgenParserT__14           = 15
	RipgenParserT__15           = 16
	RipgenParserT__16           = 17
	RipgenParserT__17           = 18
	RipgenParserT__18           = 19
	RipgenParserT__19           = 20
	RipgenParserT__20           = 21
	RipgenParserT__21           = 22
	RipgenParserT__22           = 23
	RipgenParserCOL             = 24
	RipgenParserSTAR            = 25
	RipgenParserNULLABLE        = 26
	RipgenParserCOMMENT         = 27
	RipgenParserTYPE_IDENTIFIER = 28
	RipgenParserPROP_IDENTIFIER = 29
	RipgenParserSTRING_LITERAL  = 30
	RipgenParserWHITESPACE      = 31
)

// RipgenParser rules.
const (
	RipgenParserRULE_start           = 0
	RipgenParserRULE_typeIdentifier  = 1
	RipgenParserRULE_propIdentifier  = 2
	RipgenParserRULE_identifier      = 3
	RipgenParserRULE_mimeType        = 4
	RipgenParserRULE_root            = 5
	RipgenParserRULE_mutation        = 6
	RipgenParserRULE_query           = 7
	RipgenParserRULE_stream          = 8
	RipgenParserRULE_namedType       = 9
	RipgenParserRULE_typeDeclaration = 10
	RipgenParserRULE_parameters      = 11
	RipgenParserRULE_keyValue        = 12
	RipgenParserRULE_subTypeSlot     = 13
	RipgenParserRULE_topTypeSlot     = 14
	RipgenParserRULE_subTypeDef      = 15
	RipgenParserRULE_topTypeDef      = 16
	RipgenParserRULE_intType         = 17
	RipgenParserRULE_floatType       = 18
	RipgenParserRULE_boolType        = 19
	RipgenParserRULE_externalType    = 20
	RipgenParserRULE_stringType      = 21
	RipgenParserRULE_anyType         = 22
	RipgenParserRULE_arrayType       = 23
	RipgenParserRULE_mapType         = 24
	RipgenParserRULE_objectType      = 25
	RipgenParserRULE_objectElement   = 26
	RipgenParserRULE_enumType        = 27
	RipgenParserRULE_enumValue       = 28
	RipgenParserRULE_customType      = 29
)

// IStartContext is an interface to support dynamic dispatch.
type IStartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Root() IRootContext
	EOF() antlr.TerminalNode

	// IsStartContext differentiates from other interfaces.
	IsStartContext()
}

type StartContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartContext() *StartContext {
	var p = new(StartContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_start
	return p
}

func InitEmptyStartContext(p *StartContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_start
}

func (*StartContext) IsStartContext() {}

func NewStartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartContext {
	var p = new(StartContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_start

	return p
}

func (s *StartContext) GetParser() antlr.Parser { return s.parser }

func (s *StartContext) Root() IRootContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IRootContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IRootContext)
}

func (s *StartContext) EOF() antlr.TerminalNode {
	return s.GetToken(RipgenParserEOF, 0)
}

func (s *StartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitStart(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Start_() (localctx IStartContext) {
	localctx = NewStartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, RipgenParserRULE_start)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		p.Root()
	}
	{
		p.SetState(61)
		p.Match(RipgenParserEOF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITypeIdentifierContext is an interface to support dynamic dispatch.
type ITypeIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	TYPE_IDENTIFIER() antlr.TerminalNode

	// IsTypeIdentifierContext differentiates from other interfaces.
	IsTypeIdentifierContext()
}

type TypeIdentifierContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTypeIdentifierContext() *TypeIdentifierContext {
	var p = new(TypeIdentifierContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_typeIdentifier
	return p
}

func InitEmptyTypeIdentifierContext(p *TypeIdentifierContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_typeIdentifier
}

func (*TypeIdentifierContext) IsTypeIdentifierContext() {}

func NewTypeIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeIdentifierContext {
	var p = new(TypeIdentifierContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_typeIdentifier

	return p
}

func (s *TypeIdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeIdentifierContext) TYPE_IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(RipgenParserTYPE_IDENTIFIER, 0)
}

func (s *TypeIdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeIdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeIdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTypeIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TypeIdentifier() (localctx ITypeIdentifierContext) {
	localctx = NewTypeIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, RipgenParserRULE_typeIdentifier)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(63)
		p.Match(RipgenParserTYPE_IDENTIFIER)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IPropIdentifierContext is an interface to support dynamic dispatch.
type IPropIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	PROP_IDENTIFIER() antlr.TerminalNode

	// IsPropIdentifierContext differentiates from other interfaces.
	IsPropIdentifierContext()
}

type PropIdentifierContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPropIdentifierContext() *PropIdentifierContext {
	var p = new(PropIdentifierContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_propIdentifier
	return p
}

func InitEmptyPropIdentifierContext(p *PropIdentifierContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_propIdentifier
}

func (*PropIdentifierContext) IsPropIdentifierContext() {}

func NewPropIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PropIdentifierContext {
	var p = new(PropIdentifierContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_propIdentifier

	return p
}

func (s *PropIdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *PropIdentifierContext) PROP_IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(RipgenParserPROP_IDENTIFIER, 0)
}

func (s *PropIdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PropIdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PropIdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitPropIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) PropIdentifier() (localctx IPropIdentifierContext) {
	localctx = NewPropIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, RipgenParserRULE_propIdentifier)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(65)
		_la = p.GetTokenStream().LA(1)

		if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&536872958) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	TypeIdentifier() ITypeIdentifierContext
	PropIdentifier() IPropIdentifierContext

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_identifier
	return p
}

func InitEmptyIdentifierContext(p *IdentifierContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_identifier
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) TypeIdentifier() ITypeIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *IdentifierContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, RipgenParserRULE_identifier)
	p.SetState(69)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case RipgenParserTYPE_IDENTIFIER:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(67)
			p.TypeIdentifier()
		}

	case RipgenParserT__0, RipgenParserT__1, RipgenParserT__2, RipgenParserT__3, RipgenParserT__4, RipgenParserT__5, RipgenParserT__6, RipgenParserT__7, RipgenParserT__8, RipgenParserT__9, RipgenParserPROP_IDENTIFIER:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(68)
			p.PropIdentifier()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IMimeTypeContext is an interface to support dynamic dispatch.
type IMimeTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllIdentifier() []IIdentifierContext
	Identifier(i int) IIdentifierContext

	// IsMimeTypeContext differentiates from other interfaces.
	IsMimeTypeContext()
}

type MimeTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMimeTypeContext() *MimeTypeContext {
	var p = new(MimeTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mimeType
	return p
}

func InitEmptyMimeTypeContext(p *MimeTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mimeType
}

func (*MimeTypeContext) IsMimeTypeContext() {}

func NewMimeTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MimeTypeContext {
	var p = new(MimeTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_mimeType

	return p
}

func (s *MimeTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *MimeTypeContext) AllIdentifier() []IIdentifierContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IIdentifierContext); ok {
			len++
		}
	}

	tst := make([]IIdentifierContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IIdentifierContext); ok {
			tst[i] = t.(IIdentifierContext)
			i++
		}
	}

	return tst
}

func (s *MimeTypeContext) Identifier(i int) IIdentifierContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *MimeTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MimeTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MimeTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMimeType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) MimeType() (localctx IMimeTypeContext) {
	localctx = NewMimeTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, RipgenParserRULE_mimeType)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(71)
		p.Identifier()
	}
	{
		p.SetState(72)
		p.Match(RipgenParserT__10)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(73)
		p.Identifier()
	}
	p.SetState(78)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&28672) != 0 {
		{
			p.SetState(74)
			_la = p.GetTokenStream().LA(1)

			if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&28672) != 0) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(75)
			p.Identifier()
		}

		p.SetState(80)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IRootContext is an interface to support dynamic dispatch.
type IRootContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllTypeDeclaration() []ITypeDeclarationContext
	TypeDeclaration(i int) ITypeDeclarationContext
	AllMutation() []IMutationContext
	Mutation(i int) IMutationContext
	AllQuery() []IQueryContext
	Query(i int) IQueryContext
	AllStream() []IStreamContext
	Stream(i int) IStreamContext

	// IsRootContext differentiates from other interfaces.
	IsRootContext()
}

type RootContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootContext() *RootContext {
	var p = new(RootContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_root
	return p
}

func InitEmptyRootContext(p *RootContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_root
}

func (*RootContext) IsRootContext() {}

func NewRootContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootContext {
	var p = new(RootContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_root

	return p
}

func (s *RootContext) GetParser() antlr.Parser { return s.parser }

func (s *RootContext) AllTypeDeclaration() []ITypeDeclarationContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(ITypeDeclarationContext); ok {
			len++
		}
	}

	tst := make([]ITypeDeclarationContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(ITypeDeclarationContext); ok {
			tst[i] = t.(ITypeDeclarationContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) TypeDeclaration(i int) ITypeDeclarationContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeDeclarationContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeDeclarationContext)
}

func (s *RootContext) AllMutation() []IMutationContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IMutationContext); ok {
			len++
		}
	}

	tst := make([]IMutationContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IMutationContext); ok {
			tst[i] = t.(IMutationContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Mutation(i int) IMutationContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IMutationContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IMutationContext)
}

func (s *RootContext) AllQuery() []IQueryContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IQueryContext); ok {
			len++
		}
	}

	tst := make([]IQueryContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IQueryContext); ok {
			tst[i] = t.(IQueryContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Query(i int) IQueryContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IQueryContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IQueryContext)
}

func (s *RootContext) AllStream() []IStreamContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IStreamContext); ok {
			len++
		}
	}

	tst := make([]IStreamContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IStreamContext); ok {
			tst[i] = t.(IStreamContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Stream(i int) IStreamContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStreamContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStreamContext)
}

func (s *RootContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitRoot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Root() (localctx IRootContext) {
	localctx = NewRootContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, RipgenParserRULE_root)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(87)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&134219648) != 0 {
		p.SetState(85)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}

		switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 2, p.GetParserRuleContext()) {
		case 1:
			{
				p.SetState(81)
				p.TypeDeclaration()
			}

		case 2:
			{
				p.SetState(82)
				p.Mutation()
			}

		case 3:
			{
				p.SetState(83)
				p.Query()
			}

		case 4:
			{
				p.SetState(84)
				p.Stream()
			}

		case antlr.ATNInvalidAltNumber:
			goto errorExit
		}

		p.SetState(89)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IMutationContext is an interface to support dynamic dispatch.
type IMutationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name rule contexts.
	GetName() IPropIdentifierContext

	// GetResponse returns the response rule contexts.
	GetResponse() ITopTypeSlotContext

	// SetName sets the name rule contexts.
	SetName(IPropIdentifierContext)

	// SetResponse sets the response rule contexts.
	SetResponse(ITopTypeSlotContext)

	// Getter signatures
	PropIdentifier() IPropIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	Parameters() IParametersContext
	COL() antlr.TerminalNode
	TopTypeSlot() ITopTypeSlotContext

	// IsMutationContext differentiates from other interfaces.
	IsMutationContext()
}

type MutationContext struct {
	antlr.BaseParserRuleContext
	parser   antlr.Parser
	name     IPropIdentifierContext
	response ITopTypeSlotContext
}

func NewEmptyMutationContext() *MutationContext {
	var p = new(MutationContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mutation
	return p
}

func InitEmptyMutationContext(p *MutationContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mutation
}

func (*MutationContext) IsMutationContext() {}

func NewMutationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MutationContext {
	var p = new(MutationContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_mutation

	return p
}

func (s *MutationContext) GetParser() antlr.Parser { return s.parser }

func (s *MutationContext) GetName() IPropIdentifierContext { return s.name }

func (s *MutationContext) GetResponse() ITopTypeSlotContext { return s.response }

func (s *MutationContext) SetName(v IPropIdentifierContext) { s.name = v }

func (s *MutationContext) SetResponse(v ITopTypeSlotContext) { s.response = v }

func (s *MutationContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *MutationContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(RipgenParserCOMMENT)
}

func (s *MutationContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(RipgenParserCOMMENT, i)
}

func (s *MutationContext) Parameters() IParametersContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParametersContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParametersContext)
}

func (s *MutationContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *MutationContext) TopTypeSlot() ITopTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITopTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITopTypeSlotContext)
}

func (s *MutationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MutationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MutationContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMutation(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Mutation() (localctx IMutationContext) {
	localctx = NewMutationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, RipgenParserRULE_mutation)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(93)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserCOMMENT {
		{
			p.SetState(90)
			p.Match(RipgenParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(95)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(96)
		p.Match(RipgenParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(97)

		var _x = p.PropIdentifier()

		localctx.(*MutationContext).name = _x
	}
	{
		p.SetState(98)
		p.Match(RipgenParserT__14)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(100)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&536872958) != 0 {
		{
			p.SetState(99)
			p.Parameters()
		}

	}
	{
		p.SetState(102)
		p.Match(RipgenParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(105)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserCOL {
		{
			p.SetState(103)
			p.Match(RipgenParserCOL)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(104)

			var _x = p.TopTypeSlot()

			localctx.(*MutationContext).response = _x
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IQueryContext is an interface to support dynamic dispatch.
type IQueryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name rule contexts.
	GetName() IPropIdentifierContext

	// GetResponse returns the response rule contexts.
	GetResponse() ITopTypeSlotContext

	// SetName sets the name rule contexts.
	SetName(IPropIdentifierContext)

	// SetResponse sets the response rule contexts.
	SetResponse(ITopTypeSlotContext)

	// Getter signatures
	COL() antlr.TerminalNode
	PropIdentifier() IPropIdentifierContext
	TopTypeSlot() ITopTypeSlotContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	Parameters() IParametersContext

	// IsQueryContext differentiates from other interfaces.
	IsQueryContext()
}

type QueryContext struct {
	antlr.BaseParserRuleContext
	parser   antlr.Parser
	name     IPropIdentifierContext
	response ITopTypeSlotContext
}

func NewEmptyQueryContext() *QueryContext {
	var p = new(QueryContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_query
	return p
}

func InitEmptyQueryContext(p *QueryContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_query
}

func (*QueryContext) IsQueryContext() {}

func NewQueryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *QueryContext {
	var p = new(QueryContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_query

	return p
}

func (s *QueryContext) GetParser() antlr.Parser { return s.parser }

func (s *QueryContext) GetName() IPropIdentifierContext { return s.name }

func (s *QueryContext) GetResponse() ITopTypeSlotContext { return s.response }

func (s *QueryContext) SetName(v IPropIdentifierContext) { s.name = v }

func (s *QueryContext) SetResponse(v ITopTypeSlotContext) { s.response = v }

func (s *QueryContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *QueryContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *QueryContext) TopTypeSlot() ITopTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITopTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITopTypeSlotContext)
}

func (s *QueryContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(RipgenParserCOMMENT)
}

func (s *QueryContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(RipgenParserCOMMENT, i)
}

func (s *QueryContext) Parameters() IParametersContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParametersContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParametersContext)
}

func (s *QueryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *QueryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *QueryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitQuery(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Query() (localctx IQueryContext) {
	localctx = NewQueryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, RipgenParserRULE_query)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(110)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserCOMMENT {
		{
			p.SetState(107)
			p.Match(RipgenParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(112)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(113)
		p.Match(RipgenParserT__7)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(114)

		var _x = p.PropIdentifier()

		localctx.(*QueryContext).name = _x
	}
	{
		p.SetState(115)
		p.Match(RipgenParserT__14)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(117)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&536872958) != 0 {
		{
			p.SetState(116)
			p.Parameters()
		}

	}
	{
		p.SetState(119)
		p.Match(RipgenParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(120)
		p.Match(RipgenParserCOL)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(121)

		var _x = p.TopTypeSlot()

		localctx.(*QueryContext).response = _x
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStreamContext is an interface to support dynamic dispatch.
type IStreamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name rule contexts.
	GetName() IPropIdentifierContext

	// SetName sets the name rule contexts.
	SetName(IPropIdentifierContext)

	// Getter signatures
	PropIdentifier() IPropIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	Parameters() IParametersContext

	// IsStreamContext differentiates from other interfaces.
	IsStreamContext()
}

type StreamContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	name   IPropIdentifierContext
}

func NewEmptyStreamContext() *StreamContext {
	var p = new(StreamContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_stream
	return p
}

func InitEmptyStreamContext(p *StreamContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_stream
}

func (*StreamContext) IsStreamContext() {}

func NewStreamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StreamContext {
	var p = new(StreamContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_stream

	return p
}

func (s *StreamContext) GetParser() antlr.Parser { return s.parser }

func (s *StreamContext) GetName() IPropIdentifierContext { return s.name }

func (s *StreamContext) SetName(v IPropIdentifierContext) { s.name = v }

func (s *StreamContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *StreamContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(RipgenParserCOMMENT)
}

func (s *StreamContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(RipgenParserCOMMENT, i)
}

func (s *StreamContext) Parameters() IParametersContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IParametersContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IParametersContext)
}

func (s *StreamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StreamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StreamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitStream(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Stream() (localctx IStreamContext) {
	localctx = NewStreamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, RipgenParserRULE_stream)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(126)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserCOMMENT {
		{
			p.SetState(123)
			p.Match(RipgenParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(128)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(129)
		p.Match(RipgenParserT__9)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(130)

		var _x = p.PropIdentifier()

		localctx.(*StreamContext).name = _x
	}
	{
		p.SetState(131)
		p.Match(RipgenParserT__14)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(133)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&536872958) != 0 {
		{
			p.SetState(132)
			p.Parameters()
		}

	}
	{
		p.SetState(135)
		p.Match(RipgenParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// INamedTypeContext is an interface to support dynamic dispatch.
type INamedTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	PropIdentifier() IPropIdentifierContext
	SubTypeSlot() ISubTypeSlotContext
	COMMENT() antlr.TerminalNode

	// IsNamedTypeContext differentiates from other interfaces.
	IsNamedTypeContext()
}

type NamedTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyNamedTypeContext() *NamedTypeContext {
	var p = new(NamedTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_namedType
	return p
}

func InitEmptyNamedTypeContext(p *NamedTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_namedType
}

func (*NamedTypeContext) IsNamedTypeContext() {}

func NewNamedTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *NamedTypeContext {
	var p = new(NamedTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_namedType

	return p
}

func (s *NamedTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *NamedTypeContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *NamedTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *NamedTypeContext) COMMENT() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOMMENT, 0)
}

func (s *NamedTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NamedTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *NamedTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitNamedType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) NamedType() (localctx INamedTypeContext) {
	localctx = NewNamedTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, RipgenParserRULE_namedType)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(137)
		p.PropIdentifier()
	}
	{
		p.SetState(138)
		p.SubTypeSlot()
	}
	p.SetState(140)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserCOMMENT {
		{
			p.SetState(139)
			p.Match(RipgenParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITypeDeclarationContext is an interface to support dynamic dispatch.
type ITypeDeclarationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name rule contexts.
	GetName() ITypeIdentifierContext

	// SetName sets the name rule contexts.
	SetName(ITypeIdentifierContext)

	// Getter signatures
	TopTypeDef() ITopTypeDefContext
	TypeIdentifier() ITypeIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode

	// IsTypeDeclarationContext differentiates from other interfaces.
	IsTypeDeclarationContext()
}

type TypeDeclarationContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	name   ITypeIdentifierContext
}

func NewEmptyTypeDeclarationContext() *TypeDeclarationContext {
	var p = new(TypeDeclarationContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_typeDeclaration
	return p
}

func InitEmptyTypeDeclarationContext(p *TypeDeclarationContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_typeDeclaration
}

func (*TypeDeclarationContext) IsTypeDeclarationContext() {}

func NewTypeDeclarationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeDeclarationContext {
	var p = new(TypeDeclarationContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_typeDeclaration

	return p
}

func (s *TypeDeclarationContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeDeclarationContext) GetName() ITypeIdentifierContext { return s.name }

func (s *TypeDeclarationContext) SetName(v ITypeIdentifierContext) { s.name = v }

func (s *TypeDeclarationContext) TopTypeDef() ITopTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITopTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITopTypeDefContext)
}

func (s *TypeDeclarationContext) TypeIdentifier() ITypeIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *TypeDeclarationContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(RipgenParserCOMMENT)
}

func (s *TypeDeclarationContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(RipgenParserCOMMENT, i)
}

func (s *TypeDeclarationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeDeclarationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeDeclarationContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTypeDeclaration(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TypeDeclaration() (localctx ITypeDeclarationContext) {
	localctx = NewTypeDeclarationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, RipgenParserRULE_typeDeclaration)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(145)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserCOMMENT {
		{
			p.SetState(142)
			p.Match(RipgenParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(147)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(148)
		p.Match(RipgenParserT__6)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(149)

		var _x = p.TypeIdentifier()

		localctx.(*TypeDeclarationContext).name = _x
	}
	{
		p.SetState(150)
		p.TopTypeDef()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IParametersContext is an interface to support dynamic dispatch.
type IParametersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllKeyValue() []IKeyValueContext
	KeyValue(i int) IKeyValueContext

	// IsParametersContext differentiates from other interfaces.
	IsParametersContext()
}

type ParametersContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyParametersContext() *ParametersContext {
	var p = new(ParametersContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_parameters
	return p
}

func InitEmptyParametersContext(p *ParametersContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_parameters
}

func (*ParametersContext) IsParametersContext() {}

func NewParametersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ParametersContext {
	var p = new(ParametersContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_parameters

	return p
}

func (s *ParametersContext) GetParser() antlr.Parser { return s.parser }

func (s *ParametersContext) AllKeyValue() []IKeyValueContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IKeyValueContext); ok {
			len++
		}
	}

	tst := make([]IKeyValueContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IKeyValueContext); ok {
			tst[i] = t.(IKeyValueContext)
			i++
		}
	}

	return tst
}

func (s *ParametersContext) KeyValue(i int) IKeyValueContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IKeyValueContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IKeyValueContext)
}

func (s *ParametersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParametersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ParametersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitParameters(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Parameters() (localctx IParametersContext) {
	localctx = NewParametersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, RipgenParserRULE_parameters)
	var _la int

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(157)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 13, p.GetParserRuleContext())
	if p.HasError() {
		goto errorExit
	}
	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			{
				p.SetState(152)
				p.KeyValue()
			}
			{
				p.SetState(153)
				p.Match(RipgenParserT__16)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		p.SetState(159)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 13, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
	}
	{
		p.SetState(160)
		p.KeyValue()
	}
	p.SetState(162)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserT__16 {
		{
			p.SetState(161)
			p.Match(RipgenParserT__16)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IKeyValueContext is an interface to support dynamic dispatch.
type IKeyValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	PropIdentifier() IPropIdentifierContext
	SubTypeSlot() ISubTypeSlotContext

	// IsKeyValueContext differentiates from other interfaces.
	IsKeyValueContext()
}

type KeyValueContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyKeyValueContext() *KeyValueContext {
	var p = new(KeyValueContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_keyValue
	return p
}

func InitEmptyKeyValueContext(p *KeyValueContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_keyValue
}

func (*KeyValueContext) IsKeyValueContext() {}

func NewKeyValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *KeyValueContext {
	var p = new(KeyValueContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_keyValue

	return p
}

func (s *KeyValueContext) GetParser() antlr.Parser { return s.parser }

func (s *KeyValueContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *KeyValueContext) SubTypeSlot() ISubTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *KeyValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *KeyValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *KeyValueContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitKeyValue(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) KeyValue() (localctx IKeyValueContext) {
	localctx = NewKeyValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, RipgenParserRULE_keyValue)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(164)
		p.PropIdentifier()
	}
	{
		p.SetState(165)
		p.SubTypeSlot()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISubTypeSlotContext is an interface to support dynamic dispatch.
type ISubTypeSlotContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	SubTypeDef() ISubTypeDefContext
	NULLABLE() antlr.TerminalNode

	// IsSubTypeSlotContext differentiates from other interfaces.
	IsSubTypeSlotContext()
}

type SubTypeSlotContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySubTypeSlotContext() *SubTypeSlotContext {
	var p = new(SubTypeSlotContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeSlot
	return p
}

func InitEmptySubTypeSlotContext(p *SubTypeSlotContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeSlot
}

func (*SubTypeSlotContext) IsSubTypeSlotContext() {}

func NewSubTypeSlotContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SubTypeSlotContext {
	var p = new(SubTypeSlotContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_subTypeSlot

	return p
}

func (s *SubTypeSlotContext) GetParser() antlr.Parser { return s.parser }

func (s *SubTypeSlotContext) SubTypeDef() ISubTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeDefContext)
}

func (s *SubTypeSlotContext) NULLABLE() antlr.TerminalNode {
	return s.GetToken(RipgenParserNULLABLE, 0)
}

func (s *SubTypeSlotContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubTypeSlotContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SubTypeSlotContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitSubTypeSlot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) SubTypeSlot() (localctx ISubTypeSlotContext) {
	localctx = NewSubTypeSlotContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, RipgenParserRULE_subTypeSlot)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(167)
		p.SubTypeDef()
	}
	p.SetState(169)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserNULLABLE {
		{
			p.SetState(168)
			p.Match(RipgenParserNULLABLE)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITopTypeSlotContext is an interface to support dynamic dispatch.
type ITopTypeSlotContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	TopTypeDef() ITopTypeDefContext
	NULLABLE() antlr.TerminalNode

	// IsTopTypeSlotContext differentiates from other interfaces.
	IsTopTypeSlotContext()
}

type TopTypeSlotContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTopTypeSlotContext() *TopTypeSlotContext {
	var p = new(TopTypeSlotContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeSlot
	return p
}

func InitEmptyTopTypeSlotContext(p *TopTypeSlotContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeSlot
}

func (*TopTypeSlotContext) IsTopTypeSlotContext() {}

func NewTopTypeSlotContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TopTypeSlotContext {
	var p = new(TopTypeSlotContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_topTypeSlot

	return p
}

func (s *TopTypeSlotContext) GetParser() antlr.Parser { return s.parser }

func (s *TopTypeSlotContext) TopTypeDef() ITopTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITopTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITopTypeDefContext)
}

func (s *TopTypeSlotContext) NULLABLE() antlr.TerminalNode {
	return s.GetToken(RipgenParserNULLABLE, 0)
}

func (s *TopTypeSlotContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TopTypeSlotContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TopTypeSlotContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTopTypeSlot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TopTypeSlot() (localctx ITopTypeSlotContext) {
	localctx = NewTopTypeSlotContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, RipgenParserRULE_topTypeSlot)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(171)
		p.TopTypeDef()
	}
	p.SetState(173)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserNULLABLE {
		{
			p.SetState(172)
			p.Match(RipgenParserNULLABLE)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISubTypeDefContext is an interface to support dynamic dispatch.
type ISubTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IntType() IIntTypeContext
	FloatType() IFloatTypeContext
	StringType() IStringTypeContext
	ArrayType() IArrayTypeContext
	MapType() IMapTypeContext
	BoolType() IBoolTypeContext
	AnyType() IAnyTypeContext
	CustomType() ICustomTypeContext

	// IsSubTypeDefContext differentiates from other interfaces.
	IsSubTypeDefContext()
}

type SubTypeDefContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySubTypeDefContext() *SubTypeDefContext {
	var p = new(SubTypeDefContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeDef
	return p
}

func InitEmptySubTypeDefContext(p *SubTypeDefContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeDef
}

func (*SubTypeDefContext) IsSubTypeDefContext() {}

func NewSubTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SubTypeDefContext {
	var p = new(SubTypeDefContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_subTypeDef

	return p
}

func (s *SubTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *SubTypeDefContext) IntType() IIntTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIntTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIntTypeContext)
}

func (s *SubTypeDefContext) FloatType() IFloatTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFloatTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFloatTypeContext)
}

func (s *SubTypeDefContext) StringType() IStringTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStringTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStringTypeContext)
}

func (s *SubTypeDefContext) ArrayType() IArrayTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IArrayTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IArrayTypeContext)
}

func (s *SubTypeDefContext) MapType() IMapTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IMapTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IMapTypeContext)
}

func (s *SubTypeDefContext) BoolType() IBoolTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IBoolTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IBoolTypeContext)
}

func (s *SubTypeDefContext) AnyType() IAnyTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IAnyTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IAnyTypeContext)
}

func (s *SubTypeDefContext) CustomType() ICustomTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ICustomTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ICustomTypeContext)
}

func (s *SubTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SubTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitSubTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) SubTypeDef() (localctx ISubTypeDefContext) {
	localctx = NewSubTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, RipgenParserRULE_subTypeDef)
	p.SetState(183)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case RipgenParserT__0:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(175)
			p.IntType()
		}

	case RipgenParserT__1:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(176)
			p.FloatType()
		}

	case RipgenParserT__2:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(177)
			p.StringType()
		}

	case RipgenParserT__18:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(178)
			p.ArrayType()
		}

	case RipgenParserT__20:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(179)
			p.MapType()
		}

	case RipgenParserT__3:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(180)
			p.BoolType()
		}

	case RipgenParserT__5:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(181)
			p.AnyType()
		}

	case RipgenParserTYPE_IDENTIFIER:
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(182)
			p.CustomType()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITopTypeDefContext is an interface to support dynamic dispatch.
type ITopTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	ObjectType() IObjectTypeContext
	EnumType() IEnumTypeContext
	ExternalType() IExternalTypeContext
	SubTypeDef() ISubTypeDefContext

	// IsTopTypeDefContext differentiates from other interfaces.
	IsTopTypeDefContext()
}

type TopTypeDefContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTopTypeDefContext() *TopTypeDefContext {
	var p = new(TopTypeDefContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeDef
	return p
}

func InitEmptyTopTypeDefContext(p *TopTypeDefContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeDef
}

func (*TopTypeDefContext) IsTopTypeDefContext() {}

func NewTopTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TopTypeDefContext {
	var p = new(TopTypeDefContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_topTypeDef

	return p
}

func (s *TopTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *TopTypeDefContext) ObjectType() IObjectTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IObjectTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IObjectTypeContext)
}

func (s *TopTypeDefContext) EnumType() IEnumTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumTypeContext)
}

func (s *TopTypeDefContext) ExternalType() IExternalTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExternalTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExternalTypeContext)
}

func (s *TopTypeDefContext) SubTypeDef() ISubTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeDefContext)
}

func (s *TopTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TopTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TopTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTopTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TopTypeDef() (localctx ITopTypeDefContext) {
	localctx = NewTopTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, RipgenParserRULE_topTypeDef)
	p.SetState(189)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 18, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(185)
			p.ObjectType()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(186)
			p.EnumType()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(187)
			p.ExternalType()
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(188)
			p.SubTypeDef()
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIntTypeContext is an interface to support dynamic dispatch.
type IIntTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsIntTypeContext differentiates from other interfaces.
	IsIntTypeContext()
}

type IntTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntTypeContext() *IntTypeContext {
	var p = new(IntTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_intType
	return p
}

func InitEmptyIntTypeContext(p *IntTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_intType
}

func (*IntTypeContext) IsIntTypeContext() {}

func NewIntTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntTypeContext {
	var p = new(IntTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_intType

	return p
}

func (s *IntTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *IntTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitIntType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) IntType() (localctx IIntTypeContext) {
	localctx = NewIntTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, RipgenParserRULE_intType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(191)
		p.Match(RipgenParserT__0)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IFloatTypeContext is an interface to support dynamic dispatch.
type IFloatTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsFloatTypeContext differentiates from other interfaces.
	IsFloatTypeContext()
}

type FloatTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatTypeContext() *FloatTypeContext {
	var p = new(FloatTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_floatType
	return p
}

func InitEmptyFloatTypeContext(p *FloatTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_floatType
}

func (*FloatTypeContext) IsFloatTypeContext() {}

func NewFloatTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatTypeContext {
	var p = new(FloatTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_floatType

	return p
}

func (s *FloatTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *FloatTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitFloatType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) FloatType() (localctx IFloatTypeContext) {
	localctx = NewFloatTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, RipgenParserRULE_floatType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(193)
		p.Match(RipgenParserT__1)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IBoolTypeContext is an interface to support dynamic dispatch.
type IBoolTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsBoolTypeContext differentiates from other interfaces.
	IsBoolTypeContext()
}

type BoolTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolTypeContext() *BoolTypeContext {
	var p = new(BoolTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_boolType
	return p
}

func InitEmptyBoolTypeContext(p *BoolTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_boolType
}

func (*BoolTypeContext) IsBoolTypeContext() {}

func NewBoolTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolTypeContext {
	var p = new(BoolTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_boolType

	return p
}

func (s *BoolTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitBoolType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) BoolType() (localctx IBoolTypeContext) {
	localctx = NewBoolTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, RipgenParserRULE_boolType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(195)
		p.Match(RipgenParserT__3)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IExternalTypeContext is an interface to support dynamic dispatch.
type IExternalTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsExternalTypeContext differentiates from other interfaces.
	IsExternalTypeContext()
}

type ExternalTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExternalTypeContext() *ExternalTypeContext {
	var p = new(ExternalTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_externalType
	return p
}

func InitEmptyExternalTypeContext(p *ExternalTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_externalType
}

func (*ExternalTypeContext) IsExternalTypeContext() {}

func NewExternalTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExternalTypeContext {
	var p = new(ExternalTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_externalType

	return p
}

func (s *ExternalTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *ExternalTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExternalTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ExternalTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitExternalType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ExternalType() (localctx IExternalTypeContext) {
	localctx = NewExternalTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, RipgenParserRULE_externalType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(197)
		p.Match(RipgenParserT__17)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStringTypeContext is an interface to support dynamic dispatch.
type IStringTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsStringTypeContext differentiates from other interfaces.
	IsStringTypeContext()
}

type StringTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringTypeContext() *StringTypeContext {
	var p = new(StringTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_stringType
	return p
}

func InitEmptyStringTypeContext(p *StringTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_stringType
}

func (*StringTypeContext) IsStringTypeContext() {}

func NewStringTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringTypeContext {
	var p = new(StringTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_stringType

	return p
}

func (s *StringTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *StringTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitStringType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) StringType() (localctx IStringTypeContext) {
	localctx = NewStringTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, RipgenParserRULE_stringType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(199)
		p.Match(RipgenParserT__2)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IAnyTypeContext is an interface to support dynamic dispatch.
type IAnyTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsAnyTypeContext differentiates from other interfaces.
	IsAnyTypeContext()
}

type AnyTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAnyTypeContext() *AnyTypeContext {
	var p = new(AnyTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_anyType
	return p
}

func InitEmptyAnyTypeContext(p *AnyTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_anyType
}

func (*AnyTypeContext) IsAnyTypeContext() {}

func NewAnyTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AnyTypeContext {
	var p = new(AnyTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_anyType

	return p
}

func (s *AnyTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *AnyTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AnyTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AnyTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitAnyType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) AnyType() (localctx IAnyTypeContext) {
	localctx = NewAnyTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, RipgenParserRULE_anyType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(201)
		p.Match(RipgenParserT__5)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IArrayTypeContext is an interface to support dynamic dispatch.
type IArrayTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	SubTypeSlot() ISubTypeSlotContext

	// IsArrayTypeContext differentiates from other interfaces.
	IsArrayTypeContext()
}

type ArrayTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyArrayTypeContext() *ArrayTypeContext {
	var p = new(ArrayTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_arrayType
	return p
}

func InitEmptyArrayTypeContext(p *ArrayTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_arrayType
}

func (*ArrayTypeContext) IsArrayTypeContext() {}

func NewArrayTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ArrayTypeContext {
	var p = new(ArrayTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_arrayType

	return p
}

func (s *ArrayTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ArrayTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *ArrayTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArrayTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ArrayTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitArrayType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ArrayType() (localctx IArrayTypeContext) {
	localctx = NewArrayTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 46, RipgenParserRULE_arrayType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(203)
		p.Match(RipgenParserT__18)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(204)
		p.SubTypeSlot()
	}
	{
		p.SetState(205)
		p.Match(RipgenParserT__19)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IMapTypeContext is an interface to support dynamic dispatch.
type IMapTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	SubTypeSlot() ISubTypeSlotContext

	// IsMapTypeContext differentiates from other interfaces.
	IsMapTypeContext()
}

type MapTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMapTypeContext() *MapTypeContext {
	var p = new(MapTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mapType
	return p
}

func InitEmptyMapTypeContext(p *MapTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_mapType
}

func (*MapTypeContext) IsMapTypeContext() {}

func NewMapTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MapTypeContext {
	var p = new(MapTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_mapType

	return p
}

func (s *MapTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *MapTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISubTypeSlotContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *MapTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MapTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MapTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMapType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) MapType() (localctx IMapTypeContext) {
	localctx = NewMapTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, RipgenParserRULE_mapType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(207)
		p.Match(RipgenParserT__20)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(208)
		p.SubTypeSlot()
	}
	{
		p.SetState(209)
		p.Match(RipgenParserT__21)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IObjectTypeContext is an interface to support dynamic dispatch.
type IObjectTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllObjectElement() []IObjectElementContext
	ObjectElement(i int) IObjectElementContext

	// IsObjectTypeContext differentiates from other interfaces.
	IsObjectTypeContext()
}

type ObjectTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyObjectTypeContext() *ObjectTypeContext {
	var p = new(ObjectTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_objectType
	return p
}

func InitEmptyObjectTypeContext(p *ObjectTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_objectType
}

func (*ObjectTypeContext) IsObjectTypeContext() {}

func NewObjectTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectTypeContext {
	var p = new(ObjectTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_objectType

	return p
}

func (s *ObjectTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectTypeContext) AllObjectElement() []IObjectElementContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IObjectElementContext); ok {
			len++
		}
	}

	tst := make([]IObjectElementContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IObjectElementContext); ok {
			tst[i] = t.(IObjectElementContext)
			i++
		}
	}

	return tst
}

func (s *ObjectTypeContext) ObjectElement(i int) IObjectElementContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IObjectElementContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IObjectElementContext)
}

func (s *ObjectTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitObjectType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ObjectType() (localctx IObjectTypeContext) {
	localctx = NewObjectTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 50, RipgenParserRULE_objectType)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(211)
		p.Match(RipgenParserT__20)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(215)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&545261566) != 0 {
		{
			p.SetState(212)
			p.ObjectElement()
		}

		p.SetState(217)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(218)
		p.Match(RipgenParserT__21)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IObjectElementContext is an interface to support dynamic dispatch.
type IObjectElementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	NamedType() INamedTypeContext
	TypeIdentifier() ITypeIdentifierContext

	// IsObjectElementContext differentiates from other interfaces.
	IsObjectElementContext()
}

type ObjectElementContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyObjectElementContext() *ObjectElementContext {
	var p = new(ObjectElementContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_objectElement
	return p
}

func InitEmptyObjectElementContext(p *ObjectElementContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_objectElement
}

func (*ObjectElementContext) IsObjectElementContext() {}

func NewObjectElementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectElementContext {
	var p = new(ObjectElementContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_objectElement

	return p
}

func (s *ObjectElementContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectElementContext) NamedType() INamedTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(INamedTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(INamedTypeContext)
}

func (s *ObjectElementContext) TypeIdentifier() ITypeIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *ObjectElementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectElementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectElementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitObjectElement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ObjectElement() (localctx IObjectElementContext) {
	localctx = NewObjectElementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 52, RipgenParserRULE_objectElement)
	p.SetState(223)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case RipgenParserT__0, RipgenParserT__1, RipgenParserT__2, RipgenParserT__3, RipgenParserT__4, RipgenParserT__5, RipgenParserT__6, RipgenParserT__7, RipgenParserT__8, RipgenParserT__9, RipgenParserPROP_IDENTIFIER:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(220)
			p.NamedType()
		}

	case RipgenParserT__22:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(221)
			p.Match(RipgenParserT__22)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(222)
			p.TypeIdentifier()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEnumTypeContext is an interface to support dynamic dispatch.
type IEnumTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Get_enumValue returns the _enumValue rule contexts.
	Get_enumValue() IEnumValueContext

	// Set_enumValue sets the _enumValue rule contexts.
	Set_enumValue(IEnumValueContext)

	// GetValues returns the values rule context list.
	GetValues() []IEnumValueContext

	// SetValues sets the values rule context list.
	SetValues([]IEnumValueContext)

	// Getter signatures
	AllEnumValue() []IEnumValueContext
	EnumValue(i int) IEnumValueContext

	// IsEnumTypeContext differentiates from other interfaces.
	IsEnumTypeContext()
}

type EnumTypeContext struct {
	antlr.BaseParserRuleContext
	parser     antlr.Parser
	_enumValue IEnumValueContext
	values     []IEnumValueContext
}

func NewEmptyEnumTypeContext() *EnumTypeContext {
	var p = new(EnumTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_enumType
	return p
}

func InitEmptyEnumTypeContext(p *EnumTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_enumType
}

func (*EnumTypeContext) IsEnumTypeContext() {}

func NewEnumTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumTypeContext {
	var p = new(EnumTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_enumType

	return p
}

func (s *EnumTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumTypeContext) Get_enumValue() IEnumValueContext { return s._enumValue }

func (s *EnumTypeContext) Set_enumValue(v IEnumValueContext) { s._enumValue = v }

func (s *EnumTypeContext) GetValues() []IEnumValueContext { return s.values }

func (s *EnumTypeContext) SetValues(v []IEnumValueContext) { s.values = v }

func (s *EnumTypeContext) AllEnumValue() []IEnumValueContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEnumValueContext); ok {
			len++
		}
	}

	tst := make([]IEnumValueContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEnumValueContext); ok {
			tst[i] = t.(IEnumValueContext)
			i++
		}
	}

	return tst
}

func (s *EnumTypeContext) EnumValue(i int) IEnumValueContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumValueContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumValueContext)
}

func (s *EnumTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitEnumType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) EnumType() (localctx IEnumTypeContext) {
	localctx = NewEnumTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 54, RipgenParserRULE_enumType)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(225)
		p.Match(RipgenParserT__4)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(226)
		p.Match(RipgenParserT__14)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(227)

		var _x = p.EnumValue()

		localctx.(*EnumTypeContext)._enumValue = _x
	}
	localctx.(*EnumTypeContext).values = append(localctx.(*EnumTypeContext).values, localctx.(*EnumTypeContext)._enumValue)
	p.SetState(232)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserT__16 {
		{
			p.SetState(228)
			p.Match(RipgenParserT__16)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(229)

			var _x = p.EnumValue()

			localctx.(*EnumTypeContext)._enumValue = _x
		}
		localctx.(*EnumTypeContext).values = append(localctx.(*EnumTypeContext).values, localctx.(*EnumTypeContext)._enumValue)

		p.SetState(234)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(235)
		p.Match(RipgenParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEnumValueContext is an interface to support dynamic dispatch.
type IEnumValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	PropIdentifier() IPropIdentifierContext
	STRING_LITERAL() antlr.TerminalNode

	// IsEnumValueContext differentiates from other interfaces.
	IsEnumValueContext()
}

type EnumValueContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumValueContext() *EnumValueContext {
	var p = new(EnumValueContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_enumValue
	return p
}

func InitEmptyEnumValueContext(p *EnumValueContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_enumValue
}

func (*EnumValueContext) IsEnumValueContext() {}

func NewEnumValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumValueContext {
	var p = new(EnumValueContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_enumValue

	return p
}

func (s *EnumValueContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumValueContext) PropIdentifier() IPropIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IPropIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *EnumValueContext) STRING_LITERAL() antlr.TerminalNode {
	return s.GetToken(RipgenParserSTRING_LITERAL, 0)
}

func (s *EnumValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumValueContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitEnumValue(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) EnumValue() (localctx IEnumValueContext) {
	localctx = NewEnumValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 56, RipgenParserRULE_enumValue)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(237)
		p.PropIdentifier()
	}
	p.SetState(239)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserSTRING_LITERAL {
		{
			p.SetState(238)
			p.Match(RipgenParserSTRING_LITERAL)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ICustomTypeContext is an interface to support dynamic dispatch.
type ICustomTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	TypeIdentifier() ITypeIdentifierContext

	// IsCustomTypeContext differentiates from other interfaces.
	IsCustomTypeContext()
}

type CustomTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCustomTypeContext() *CustomTypeContext {
	var p = new(CustomTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_customType
	return p
}

func InitEmptyCustomTypeContext(p *CustomTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = RipgenParserRULE_customType
}

func (*CustomTypeContext) IsCustomTypeContext() {}

func NewCustomTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CustomTypeContext {
	var p = new(CustomTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_customType

	return p
}

func (s *CustomTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *CustomTypeContext) TypeIdentifier() ITypeIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *CustomTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CustomTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CustomTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCustomType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CustomType() (localctx ICustomTypeContext) {
	localctx = NewCustomTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 58, RipgenParserRULE_customType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(241)
		p.TypeIdentifier()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

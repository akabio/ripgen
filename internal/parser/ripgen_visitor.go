// Code generated from internal/parser/Ripgen.g4 by ANTLR 4.13.0. DO NOT EDIT.

package parser // Ripgen

import "github.com/antlr4-go/antlr/v4"

// A complete Visitor for a parse tree produced by RipgenParser.
type RipgenVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by RipgenParser#start.
	VisitStart(ctx *StartContext) interface{}

	// Visit a parse tree produced by RipgenParser#typeIdentifier.
	VisitTypeIdentifier(ctx *TypeIdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#propIdentifier.
	VisitPropIdentifier(ctx *PropIdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#mimeType.
	VisitMimeType(ctx *MimeTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#root.
	VisitRoot(ctx *RootContext) interface{}

	// Visit a parse tree produced by RipgenParser#mutation.
	VisitMutation(ctx *MutationContext) interface{}

	// Visit a parse tree produced by RipgenParser#query.
	VisitQuery(ctx *QueryContext) interface{}

	// Visit a parse tree produced by RipgenParser#stream.
	VisitStream(ctx *StreamContext) interface{}

	// Visit a parse tree produced by RipgenParser#namedType.
	VisitNamedType(ctx *NamedTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#typeDeclaration.
	VisitTypeDeclaration(ctx *TypeDeclarationContext) interface{}

	// Visit a parse tree produced by RipgenParser#parameters.
	VisitParameters(ctx *ParametersContext) interface{}

	// Visit a parse tree produced by RipgenParser#keyValue.
	VisitKeyValue(ctx *KeyValueContext) interface{}

	// Visit a parse tree produced by RipgenParser#subTypeSlot.
	VisitSubTypeSlot(ctx *SubTypeSlotContext) interface{}

	// Visit a parse tree produced by RipgenParser#topTypeSlot.
	VisitTopTypeSlot(ctx *TopTypeSlotContext) interface{}

	// Visit a parse tree produced by RipgenParser#subTypeDef.
	VisitSubTypeDef(ctx *SubTypeDefContext) interface{}

	// Visit a parse tree produced by RipgenParser#topTypeDef.
	VisitTopTypeDef(ctx *TopTypeDefContext) interface{}

	// Visit a parse tree produced by RipgenParser#intType.
	VisitIntType(ctx *IntTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#floatType.
	VisitFloatType(ctx *FloatTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#boolType.
	VisitBoolType(ctx *BoolTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#externalType.
	VisitExternalType(ctx *ExternalTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#stringType.
	VisitStringType(ctx *StringTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#anyType.
	VisitAnyType(ctx *AnyTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#arrayType.
	VisitArrayType(ctx *ArrayTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#mapType.
	VisitMapType(ctx *MapTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#objectType.
	VisitObjectType(ctx *ObjectTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#objectElement.
	VisitObjectElement(ctx *ObjectElementContext) interface{}

	// Visit a parse tree produced by RipgenParser#enumType.
	VisitEnumType(ctx *EnumTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#enumValue.
	VisitEnumValue(ctx *EnumValueContext) interface{}

	// Visit a parse tree produced by RipgenParser#customType.
	VisitCustomType(ctx *CustomTypeContext) interface{}
}

package parser

import (
	"fmt"
	"strings"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/ripgen/internal/ast"
	"gitlab.com/akabio/ripgen/internal/locprov"
	"gitlab.com/akabio/ripgen/model"
)

type ASTVisitor struct {
	*BaseRipgenVisitor
	AST    *ast.AST
	errors []*model.ParseError
	// name is name of current file
	Name string
}

func (v *ASTVisitor) VisitStart(ctx *StartContext) interface{} {
	return ctx.Root().Accept(v)
}

func (v *ASTVisitor) VisitRoot(ctx *RootContext) interface{} {
	for _, defDec := range ctx.AllTypeDeclaration() {
		c := defDec.Accept(v)
		v.AST.TypeDeclarations = append(v.AST.TypeDeclarations, c.(*ast.TypeDeclaration))
	}

	for _, mut := range ctx.AllMutation() {
		c := mut.Accept(v)
		v.AST.Mutations = append(v.AST.Mutations, c.(*ast.Mutation))
	}

	for _, query := range ctx.AllQuery() {
		c := query.Accept(v)
		v.AST.Queries = append(v.AST.Queries, c.(*ast.Query))
	}

	for _, stream := range ctx.AllStream() {
		c := stream.Accept(v)
		v.AST.Streams = append(v.AST.Streams, c.(*ast.Stream))
	}

	return v.AST
}

func (v *ASTVisitor) VisitMutation(ctx *MutationContext) interface{} {
	mut := &ast.Mutation{
		Comment:  comment(ctx.AllCOMMENT()),
		Name:     ctx.GetName().GetText(),
		Location: v.tokenLocation(ctx.GetStart()),
	}
	if ctx.Parameters() != nil {
		mut.Parameters = ctx.Parameters().Accept(v).([]*ast.NamedType)
	}

	if ctx.GetResponse() != nil {
		mut.Response = ctx.GetResponse().Accept(v)
	}

	return mut
}

func (v *ASTVisitor) VisitQuery(ctx *QueryContext) interface{} {
	var response interface{}
	if ctx.GetResponse() != nil {
		response = ctx.GetResponse().Accept(v)
	}

	query := &ast.Query{
		Comment:  comment(ctx.AllCOMMENT()),
		Name:     ctx.GetName().GetText(),
		Response: response,
		Location: v.tokenLocation(ctx.GetStart()),
	}

	if ctx.Parameters() != nil {
		query.Parameters = ctx.Parameters().Accept(v).([]*ast.NamedType)
	}

	return query
}

func (v *ASTVisitor) VisitStream(ctx *StreamContext) interface{} {
	stream := &ast.Stream{
		Comment:  comment(ctx.AllCOMMENT()),
		Name:     ctx.GetName().GetText(),
		Location: v.tokenLocation(ctx.GetStart()),
	}

	if ctx.Parameters() != nil {
		stream.Parameters = ctx.Parameters().Accept(v).([]*ast.NamedType)
	}

	return stream
}

func (v *ASTVisitor) VisitNamedType(ctx *NamedTypeContext) interface{} {
	t := ctx.SubTypeSlot().Accept(v)
	name := ctx.PropIdentifier().GetText()

	comment := ""
	if ctx.COMMENT() != nil {
		comment = ctx.COMMENT().GetText()[2:]
		comment = strings.Trim(comment, "\t ")
	}

	return &ast.NamedType{
		Name:     name,
		Type:     t,
		Comment:  comment,
		Location: v.tokenLocation(ctx.SubTypeSlot().GetStart()),
	}
}

func (v *ASTVisitor) VisitSubTypeDef(ctx *SubTypeDefContext) interface{} {
	if ctx.IntType() != nil {
		return &ast.IntType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.FloatType() != nil {
		return &ast.FloatType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.StringType() != nil {
		return &ast.StringType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.BoolType() != nil {
		return &ast.BoolType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.AnyType() != nil {
		return &ast.AnyType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.ArrayType() != nil {
		return ctx.ArrayType().Accept(v).(*ast.ArrayType)
	}

	if ctx.MapType() != nil {
		return ctx.MapType().Accept(v)
	}

	if ctx.CustomType() != nil {
		return ctx.CustomType().Accept(v)
	}

	v.errors = append(v.errors, v.mkError(ctx.GetStart(), fmt.Sprintf("unknown type %+v", ctx)))

	return nil
}

func (v *ASTVisitor) VisitTopTypeSlot(ctx *TopTypeSlotContext) interface{} {
	if ctx.NULLABLE() != nil {
		return &ast.NullableType{
			Type:     ctx.TopTypeDef().Accept(v),
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	return ctx.TopTypeDef().Accept(v)
}

func (v *ASTVisitor) VisitSubTypeSlot(ctx *SubTypeSlotContext) interface{} {
	if ctx.NULLABLE() != nil {
		return &ast.NullableType{
			Type:     ctx.SubTypeDef().Accept(v),
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	return ctx.SubTypeDef().Accept(v)
}

func (v *ASTVisitor) VisitTopTypeDef(ctx *TopTypeDefContext) interface{} {
	if ctx.ObjectType() != nil {
		return ctx.ObjectType().Accept(v).(*ast.ObjectType)
	}

	if ctx.EnumType() != nil {
		return ctx.EnumType().Accept(v).(*ast.EnumType)
	}

	if ctx.ExternalType() != nil {
		return &ast.ExternalType{
			Location: v.tokenLocation(ctx.GetStart()),
		}
	}

	if ctx.SubTypeDef() != nil {
		return ctx.SubTypeDef().Accept(v)
	}

	v.errors = append(v.errors, v.mkError(ctx.GetStart(), fmt.Sprintf("unknown type %+v", ctx)))

	return nil
}

func (v *ASTVisitor) VisitObjectType(ctx *ObjectTypeContext) interface{} {
	ot := &ast.ObjectType{
		Location: v.tokenLocation(ctx.GetStart()),
	}

	for _, nt := range ctx.AllObjectElement() {
		t := nt.Accept(v)
		switch tp := t.(type) {
		case *ast.NamedType:
			ot.Fields = append(ot.Fields, tp)
		case *ast.Extends:
			ot.Fields = append(ot.Fields, tp)
		default:
			v.errors = append(v.errors, v.mkError(ctx.GetStart(), fmt.Sprintf("unknown type %T", t)))
		}
	}

	return ot
}

func (v *ASTVisitor) VisitObjectElement(ctx *ObjectElementContext) interface{} {
	if ctx.NamedType() != nil {
		return ctx.NamedType().Accept(v)
	} else if ctx.TypeIdentifier() != nil {
		str := ctx.TypeIdentifier().GetText()

		return &ast.Extends{Name: str, Location: v.tokenLocation(ctx.TypeIdentifier().GetStart())}
	}

	v.errors = append(v.errors, v.mkError(ctx.GetStart(), "must be TypeIdentifier or NamedType"))

	return nil
}

func (v *ASTVisitor) VisitMapType(ctx *MapTypeContext) interface{} {
	return &ast.MapType{
		Type:     ctx.SubTypeSlot().Accept(v),
		Location: v.tokenLocation(ctx.GetStart()),
	}
}

func (v *ASTVisitor) VisitCustomType(ctx *CustomTypeContext) interface{} {
	name := ctx.TypeIdentifier().GetText()

	return &ast.CustomType{Name: name, Location: v.tokenLocation(ctx.GetStart())}
}

func (v *ASTVisitor) VisitArrayType(ctx *ArrayTypeContext) interface{} {
	return &ast.ArrayType{
		Type:     ctx.SubTypeSlot().Accept(v),
		Location: v.tokenLocation(ctx.GetStart()),
	}
}

func (v *ASTVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	vals := []ast.EnumValue{}
	for _, ev := range ctx.GetValues() {
		vals = append(vals, ev.Accept(v).(ast.EnumValue))
	}

	return &ast.EnumType{
		Values:   vals,
		Location: v.tokenLocation(ctx.GetStart()),
	}
}

func (v *ASTVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	val := ast.EnumValue{
		Name:     ctx.PropIdentifier().GetText(),
		Value:    ctx.PropIdentifier().GetText(),
		Location: v.tokenLocation(ctx.GetStart()),
	}

	if ctx.STRING_LITERAL() != nil {
		str := ctx.STRING_LITERAL().GetText()
		val.Value = str[1 : len(str)-1]
	}

	return val
}

func (v *ASTVisitor) VisitTypeDeclaration(ctx *TypeDeclarationContext) interface{} {
	name := ctx.GetName().GetText()
	tp := ctx.TopTypeDef().Accept(v)

	external, is := tp.(*ast.ExternalType)
	if is {
		external.ID = name
	}

	return &ast.TypeDeclaration{
		Comment: comment(ctx.AllCOMMENT()),
		Name:    name,
		Type:    tp,
	}
}

func (v *ASTVisitor) VisitParameters(ctx *ParametersContext) interface{} {
	params := []*ast.NamedType{}
	for _, kv := range ctx.AllKeyValue() {
		params = append(params, kv.Accept(v).(*ast.NamedType))
	}

	return params
}

func (v *ASTVisitor) VisitKeyValue(ctx *KeyValueContext) interface{} {
	t := ctx.SubTypeSlot().Accept(v)
	name := ctx.PropIdentifier().GetText()

	return &ast.NamedType{
		Name: name,
		Type: t,
	}
}

func comment(ns []antlr.TerminalNode) []string {
	r := []string{}
	for _, n := range ns {
		r = append(r, n.GetText()[2:])
	}

	r, _ = removeSpaceBlock(r)

	return r
}

func (v *ASTVisitor) mkError(tk antlr.Token, msg string) *model.ParseError {
	return &model.ParseError{
		Location: v.tokenLocation(tk),
		Message:  msg,
	}
}

func (v *ASTVisitor) tokenLocation(tk antlr.Token) *locprov.Location {
	return locprov.New(v.Name, tk.GetLine()-1, tk.GetLine()-1, tk.GetColumn(), tk.GetColumn()+len(tk.GetText()))
}

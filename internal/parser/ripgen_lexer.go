// Code generated from internal/parser/Ripgen.g4 by ANTLR 4.13.0. DO NOT EDIT.

package parser

import (
	"fmt"
	"github.com/antlr4-go/antlr/v4"
	"sync"
	"unicode"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = sync.Once{}
var _ = unicode.IsLetter

type RipgenLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var RipgenLexerLexerStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	ChannelNames           []string
	ModeNames              []string
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func ripgenlexerLexerInit() {
	staticData := &RipgenLexerLexerStaticData
	staticData.ChannelNames = []string{
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	}
	staticData.ModeNames = []string{
		"DEFAULT_MODE",
	}
	staticData.LiteralNames = []string{
		"", "'int'", "'float'", "'string'", "'bool'", "'enum'", "'any'", "'type'",
		"'query'", "'mutation'", "'stream'", "'/'", "'+'", "'-'", "'.'", "'('",
		"')'", "','", "'external'", "'['", "']'", "'{'", "'}'", "'...'", "':'",
		"'*'", "'?'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "COL", "STAR", "NULLABLE", "COMMENT", "TYPE_IDENTIFIER",
		"PROP_IDENTIFIER", "STRING_LITERAL", "WHITESPACE",
	}
	staticData.RuleNames = []string{
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "COL", "STAR",
		"NULLABLE", "COMMENT", "TYPE_IDENTIFIER", "PROP_IDENTIFIER", "STRING_LITERAL",
		"WHITESPACE",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 0, 31, 203, 6, -1, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2,
		4, 7, 4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2,
		10, 7, 10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15,
		7, 15, 2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7,
		20, 2, 21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25,
		2, 26, 7, 26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 2, 30, 7, 30, 1,
		0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1,
		2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 4, 1, 4, 1,
		4, 1, 4, 1, 4, 1, 5, 1, 5, 1, 5, 1, 5, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1,
		7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 8, 1, 8, 1, 8, 1, 8, 1, 8, 1, 8, 1,
		8, 1, 8, 1, 8, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 10, 1, 10,
		1, 11, 1, 11, 1, 12, 1, 12, 1, 13, 1, 13, 1, 14, 1, 14, 1, 15, 1, 15, 1,
		16, 1, 16, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17,
		1, 18, 1, 18, 1, 19, 1, 19, 1, 20, 1, 20, 1, 21, 1, 21, 1, 22, 1, 22, 1,
		22, 1, 22, 1, 23, 1, 23, 1, 24, 1, 24, 1, 25, 1, 25, 1, 26, 1, 26, 1, 26,
		1, 26, 5, 26, 167, 8, 26, 10, 26, 12, 26, 170, 9, 26, 1, 27, 1, 27, 5,
		27, 174, 8, 27, 10, 27, 12, 27, 177, 9, 27, 1, 28, 1, 28, 5, 28, 181, 8,
		28, 10, 28, 12, 28, 184, 9, 28, 1, 29, 1, 29, 1, 29, 1, 29, 5, 29, 190,
		8, 29, 10, 29, 12, 29, 193, 9, 29, 1, 29, 1, 29, 1, 30, 4, 30, 198, 8,
		30, 11, 30, 12, 30, 199, 1, 30, 1, 30, 0, 0, 31, 1, 1, 3, 2, 5, 3, 7, 4,
		9, 5, 11, 6, 13, 7, 15, 8, 17, 9, 19, 10, 21, 11, 23, 12, 25, 13, 27, 14,
		29, 15, 31, 16, 33, 17, 35, 18, 37, 19, 39, 20, 41, 21, 43, 22, 45, 23,
		47, 24, 49, 25, 51, 26, 53, 27, 55, 28, 57, 29, 59, 30, 61, 31, 1, 0, 6,
		2, 0, 10, 10, 13, 13, 1, 0, 65, 90, 3, 0, 48, 57, 65, 90, 97, 122, 1, 0,
		97, 122, 3, 0, 10, 10, 13, 13, 34, 34, 3, 0, 9, 10, 13, 13, 32, 32, 208,
		0, 1, 1, 0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0,
		0, 9, 1, 0, 0, 0, 0, 11, 1, 0, 0, 0, 0, 13, 1, 0, 0, 0, 0, 15, 1, 0, 0,
		0, 0, 17, 1, 0, 0, 0, 0, 19, 1, 0, 0, 0, 0, 21, 1, 0, 0, 0, 0, 23, 1, 0,
		0, 0, 0, 25, 1, 0, 0, 0, 0, 27, 1, 0, 0, 0, 0, 29, 1, 0, 0, 0, 0, 31, 1,
		0, 0, 0, 0, 33, 1, 0, 0, 0, 0, 35, 1, 0, 0, 0, 0, 37, 1, 0, 0, 0, 0, 39,
		1, 0, 0, 0, 0, 41, 1, 0, 0, 0, 0, 43, 1, 0, 0, 0, 0, 45, 1, 0, 0, 0, 0,
		47, 1, 0, 0, 0, 0, 49, 1, 0, 0, 0, 0, 51, 1, 0, 0, 0, 0, 53, 1, 0, 0, 0,
		0, 55, 1, 0, 0, 0, 0, 57, 1, 0, 0, 0, 0, 59, 1, 0, 0, 0, 0, 61, 1, 0, 0,
		0, 1, 63, 1, 0, 0, 0, 3, 67, 1, 0, 0, 0, 5, 73, 1, 0, 0, 0, 7, 80, 1, 0,
		0, 0, 9, 85, 1, 0, 0, 0, 11, 90, 1, 0, 0, 0, 13, 94, 1, 0, 0, 0, 15, 99,
		1, 0, 0, 0, 17, 105, 1, 0, 0, 0, 19, 114, 1, 0, 0, 0, 21, 121, 1, 0, 0,
		0, 23, 123, 1, 0, 0, 0, 25, 125, 1, 0, 0, 0, 27, 127, 1, 0, 0, 0, 29, 129,
		1, 0, 0, 0, 31, 131, 1, 0, 0, 0, 33, 133, 1, 0, 0, 0, 35, 135, 1, 0, 0,
		0, 37, 144, 1, 0, 0, 0, 39, 146, 1, 0, 0, 0, 41, 148, 1, 0, 0, 0, 43, 150,
		1, 0, 0, 0, 45, 152, 1, 0, 0, 0, 47, 156, 1, 0, 0, 0, 49, 158, 1, 0, 0,
		0, 51, 160, 1, 0, 0, 0, 53, 162, 1, 0, 0, 0, 55, 171, 1, 0, 0, 0, 57, 178,
		1, 0, 0, 0, 59, 185, 1, 0, 0, 0, 61, 197, 1, 0, 0, 0, 63, 64, 5, 105, 0,
		0, 64, 65, 5, 110, 0, 0, 65, 66, 5, 116, 0, 0, 66, 2, 1, 0, 0, 0, 67, 68,
		5, 102, 0, 0, 68, 69, 5, 108, 0, 0, 69, 70, 5, 111, 0, 0, 70, 71, 5, 97,
		0, 0, 71, 72, 5, 116, 0, 0, 72, 4, 1, 0, 0, 0, 73, 74, 5, 115, 0, 0, 74,
		75, 5, 116, 0, 0, 75, 76, 5, 114, 0, 0, 76, 77, 5, 105, 0, 0, 77, 78, 5,
		110, 0, 0, 78, 79, 5, 103, 0, 0, 79, 6, 1, 0, 0, 0, 80, 81, 5, 98, 0, 0,
		81, 82, 5, 111, 0, 0, 82, 83, 5, 111, 0, 0, 83, 84, 5, 108, 0, 0, 84, 8,
		1, 0, 0, 0, 85, 86, 5, 101, 0, 0, 86, 87, 5, 110, 0, 0, 87, 88, 5, 117,
		0, 0, 88, 89, 5, 109, 0, 0, 89, 10, 1, 0, 0, 0, 90, 91, 5, 97, 0, 0, 91,
		92, 5, 110, 0, 0, 92, 93, 5, 121, 0, 0, 93, 12, 1, 0, 0, 0, 94, 95, 5,
		116, 0, 0, 95, 96, 5, 121, 0, 0, 96, 97, 5, 112, 0, 0, 97, 98, 5, 101,
		0, 0, 98, 14, 1, 0, 0, 0, 99, 100, 5, 113, 0, 0, 100, 101, 5, 117, 0, 0,
		101, 102, 5, 101, 0, 0, 102, 103, 5, 114, 0, 0, 103, 104, 5, 121, 0, 0,
		104, 16, 1, 0, 0, 0, 105, 106, 5, 109, 0, 0, 106, 107, 5, 117, 0, 0, 107,
		108, 5, 116, 0, 0, 108, 109, 5, 97, 0, 0, 109, 110, 5, 116, 0, 0, 110,
		111, 5, 105, 0, 0, 111, 112, 5, 111, 0, 0, 112, 113, 5, 110, 0, 0, 113,
		18, 1, 0, 0, 0, 114, 115, 5, 115, 0, 0, 115, 116, 5, 116, 0, 0, 116, 117,
		5, 114, 0, 0, 117, 118, 5, 101, 0, 0, 118, 119, 5, 97, 0, 0, 119, 120,
		5, 109, 0, 0, 120, 20, 1, 0, 0, 0, 121, 122, 5, 47, 0, 0, 122, 22, 1, 0,
		0, 0, 123, 124, 5, 43, 0, 0, 124, 24, 1, 0, 0, 0, 125, 126, 5, 45, 0, 0,
		126, 26, 1, 0, 0, 0, 127, 128, 5, 46, 0, 0, 128, 28, 1, 0, 0, 0, 129, 130,
		5, 40, 0, 0, 130, 30, 1, 0, 0, 0, 131, 132, 5, 41, 0, 0, 132, 32, 1, 0,
		0, 0, 133, 134, 5, 44, 0, 0, 134, 34, 1, 0, 0, 0, 135, 136, 5, 101, 0,
		0, 136, 137, 5, 120, 0, 0, 137, 138, 5, 116, 0, 0, 138, 139, 5, 101, 0,
		0, 139, 140, 5, 114, 0, 0, 140, 141, 5, 110, 0, 0, 141, 142, 5, 97, 0,
		0, 142, 143, 5, 108, 0, 0, 143, 36, 1, 0, 0, 0, 144, 145, 5, 91, 0, 0,
		145, 38, 1, 0, 0, 0, 146, 147, 5, 93, 0, 0, 147, 40, 1, 0, 0, 0, 148, 149,
		5, 123, 0, 0, 149, 42, 1, 0, 0, 0, 150, 151, 5, 125, 0, 0, 151, 44, 1,
		0, 0, 0, 152, 153, 5, 46, 0, 0, 153, 154, 5, 46, 0, 0, 154, 155, 5, 46,
		0, 0, 155, 46, 1, 0, 0, 0, 156, 157, 5, 58, 0, 0, 157, 48, 1, 0, 0, 0,
		158, 159, 5, 42, 0, 0, 159, 50, 1, 0, 0, 0, 160, 161, 5, 63, 0, 0, 161,
		52, 1, 0, 0, 0, 162, 163, 5, 47, 0, 0, 163, 164, 5, 47, 0, 0, 164, 168,
		1, 0, 0, 0, 165, 167, 8, 0, 0, 0, 166, 165, 1, 0, 0, 0, 167, 170, 1, 0,
		0, 0, 168, 166, 1, 0, 0, 0, 168, 169, 1, 0, 0, 0, 169, 54, 1, 0, 0, 0,
		170, 168, 1, 0, 0, 0, 171, 175, 7, 1, 0, 0, 172, 174, 7, 2, 0, 0, 173,
		172, 1, 0, 0, 0, 174, 177, 1, 0, 0, 0, 175, 173, 1, 0, 0, 0, 175, 176,
		1, 0, 0, 0, 176, 56, 1, 0, 0, 0, 177, 175, 1, 0, 0, 0, 178, 182, 7, 3,
		0, 0, 179, 181, 7, 2, 0, 0, 180, 179, 1, 0, 0, 0, 181, 184, 1, 0, 0, 0,
		182, 180, 1, 0, 0, 0, 182, 183, 1, 0, 0, 0, 183, 58, 1, 0, 0, 0, 184, 182,
		1, 0, 0, 0, 185, 191, 5, 34, 0, 0, 186, 190, 8, 4, 0, 0, 187, 188, 5, 92,
		0, 0, 188, 190, 5, 34, 0, 0, 189, 186, 1, 0, 0, 0, 189, 187, 1, 0, 0, 0,
		190, 193, 1, 0, 0, 0, 191, 189, 1, 0, 0, 0, 191, 192, 1, 0, 0, 0, 192,
		194, 1, 0, 0, 0, 193, 191, 1, 0, 0, 0, 194, 195, 5, 34, 0, 0, 195, 60,
		1, 0, 0, 0, 196, 198, 7, 5, 0, 0, 197, 196, 1, 0, 0, 0, 198, 199, 1, 0,
		0, 0, 199, 197, 1, 0, 0, 0, 199, 200, 1, 0, 0, 0, 200, 201, 1, 0, 0, 0,
		201, 202, 6, 30, 0, 0, 202, 62, 1, 0, 0, 0, 7, 0, 168, 175, 182, 189, 191,
		199, 1, 6, 0, 0,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// RipgenLexerInit initializes any static state used to implement RipgenLexer. By default the
// static state used to implement the lexer is lazily initialized during the first call to
// NewRipgenLexer(). You can call this function if you wish to initialize the static state ahead
// of time.
func RipgenLexerInit() {
	staticData := &RipgenLexerLexerStaticData
	staticData.once.Do(ripgenlexerLexerInit)
}

// NewRipgenLexer produces a new lexer instance for the optional input antlr.CharStream.
func NewRipgenLexer(input antlr.CharStream) *RipgenLexer {
	RipgenLexerInit()
	l := new(RipgenLexer)
	l.BaseLexer = antlr.NewBaseLexer(input)
	staticData := &RipgenLexerLexerStaticData
	l.Interpreter = antlr.NewLexerATNSimulator(l, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	l.channelNames = staticData.ChannelNames
	l.modeNames = staticData.ModeNames
	l.RuleNames = staticData.RuleNames
	l.LiteralNames = staticData.LiteralNames
	l.SymbolicNames = staticData.SymbolicNames
	l.GrammarFileName = "Ripgen.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// RipgenLexer tokens.
const (
	RipgenLexerT__0            = 1
	RipgenLexerT__1            = 2
	RipgenLexerT__2            = 3
	RipgenLexerT__3            = 4
	RipgenLexerT__4            = 5
	RipgenLexerT__5            = 6
	RipgenLexerT__6            = 7
	RipgenLexerT__7            = 8
	RipgenLexerT__8            = 9
	RipgenLexerT__9            = 10
	RipgenLexerT__10           = 11
	RipgenLexerT__11           = 12
	RipgenLexerT__12           = 13
	RipgenLexerT__13           = 14
	RipgenLexerT__14           = 15
	RipgenLexerT__15           = 16
	RipgenLexerT__16           = 17
	RipgenLexerT__17           = 18
	RipgenLexerT__18           = 19
	RipgenLexerT__19           = 20
	RipgenLexerT__20           = 21
	RipgenLexerT__21           = 22
	RipgenLexerT__22           = 23
	RipgenLexerCOL             = 24
	RipgenLexerSTAR            = 25
	RipgenLexerNULLABLE        = 26
	RipgenLexerCOMMENT         = 27
	RipgenLexerTYPE_IDENTIFIER = 28
	RipgenLexerPROP_IDENTIFIER = 29
	RipgenLexerSTRING_LITERAL  = 30
	RipgenLexerWHITESPACE      = 31
)

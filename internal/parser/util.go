package parser

import (
	"regexp"
	"strings"
)

// copied from akabio/gogen/parser

var (
	countSpaces = regexp.MustCompile("^ *")
	countTabs   = regexp.MustCompile("^\t*")
)

const invalid = 10000

func removeSpaceBlock(lines []string) ([]string, int) {
	spaces := invalid
	tabs := invalid

	for _, l := range lines {
		ls := strings.TrimRight(l, " \t")
		if len(ls) > 0 { // we check only lines with some non space chars
			sp := countSpaces.FindString(ls)
			tb := countTabs.FindString(ls)

			if len(sp) != 0 && len(sp) < spaces {
				spaces = len(sp)
			}

			if len(tb) != 0 && len(tb) < tabs {
				tabs = len(tb)
			}
		}
	}

	prefix := ""
	columns := 0

	if spaces != invalid && spaces > 0 {
		prefix = strings.Repeat(" ", spaces)
		columns = spaces
	}

	if tabs != invalid && tabs > 0 {
		prefix = strings.Repeat("\t", tabs)
		columns = tabs
	}

	for i := range lines {
		lines[i] = strings.TrimPrefix(lines[i], prefix)
	}

	return lines, columns
}

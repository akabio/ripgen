package parser

import (
	"fmt"
	"os"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/ripgen/internal/locprov"
	"gitlab.com/akabio/ripgen/model"
)

type ErrorListener struct {
	name   string
	Errors []error
}

func NewErrorListener(name string) *ErrorListener {
	return &ErrorListener{name: name}
}

func (d *ErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{},
	line, column int, msg string, e antlr.RecognitionException,
) {
	tkl := 1

	switch s := offendingSymbol.(type) {
	case *antlr.CommonToken:
		tkl = len(s.GetText())
	default:
		fmt.Fprintf(os.Stderr, "unknown sytnax error symbol %T\n", offendingSymbol)
	}

	d.Errors = append(d.Errors, &model.ParseError{
		Location: locprov.New(d.name, line-1, line-1, column, column+tkl),
		Message:  msg,
	})
}

func (d *ErrorListener) ReportAmbiguity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex int,
	exact bool, ambigAlts *antlr.BitSet, configs *antlr.ATNConfigSet,
) {
}

func (d *ErrorListener) ReportAttemptingFullContext(recognizer antlr.Parser, dfa *antlr.DFA, startIndex,
	stopIndex int, conflictingAlts *antlr.BitSet, configs *antlr.ATNConfigSet) {
}

func (d *ErrorListener) ReportContextSensitivity(recognizer antlr.Parser, dfa *antlr.DFA, startIndex, stopIndex,
	prediction int, configs *antlr.ATNConfigSet) {
}

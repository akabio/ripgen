grammar Ripgen;

COL: ':';
STAR: '*';
NULLABLE: '?';

COMMENT: '//' ~( '\r' | '\n')*;

TYPE_IDENTIFIER: [A-Z][a-zA-Z0-9]*;
PROP_IDENTIFIER: [a-z][a-zA-Z0-9]*;

STRING_LITERAL: '"' (~["\r\n] | '\\"')* '"';

WHITESPACE: [ \r\n\t]+ -> skip;

// Rules
start: root EOF;

typeIdentifier: TYPE_IDENTIFIER;

propIdentifier:
	PROP_IDENTIFIER
	| 'int'
	| 'float'
	| 'string'
	| 'bool'
	| 'enum'
	| 'any'
	| 'type'
	| 'query'
	| 'mutation'
	| 'stream'
;	

identifier: typeIdentifier | propIdentifier;

mimeType:
	identifier '/' identifier (('+' | '-' | '.') identifier)*;

root: (typeDeclaration | mutation | query | stream)*;

mutation:
	COMMENT* 'mutation' name=propIdentifier '('parameters?')' (':' response=topTypeSlot)?;

query:
	COMMENT* 'query' name=propIdentifier '('parameters?')' ':' response=topTypeSlot;

stream:
	COMMENT* 'stream' name=propIdentifier '('parameters?')';


namedType: propIdentifier subTypeSlot COMMENT?;

typeDeclaration:
	COMMENT* 'type' name = typeIdentifier topTypeDef;

parameters: ((keyValue ',')* keyValue ','?);

keyValue: propIdentifier subTypeSlot;

subTypeSlot: subTypeDef NULLABLE?;

topTypeSlot: topTypeDef NULLABLE?;

subTypeDef:
	intType
	| floatType
	| stringType
	| arrayType
	| mapType
	| boolType
	| anyType
	| customType;

topTypeDef: objectType | enumType | externalType | subTypeDef;

intType: 'int';

floatType: 'float';

boolType: 'bool';

externalType: 'external';

stringType: 'string';

anyType: 'any';

arrayType: '[' subTypeSlot ']';

mapType: '{' subTypeSlot '}';

objectType: '{' objectElement* '}';

objectElement: namedType | '...' typeIdentifier;

enumType:
	'enum' '(' values += enumValue (',' values += enumValue)* ')';

enumValue: propIdentifier STRING_LITERAL?;

customType: typeIdentifier;
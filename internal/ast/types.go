package ast

import "gitlab.com/akabio/ripgen/internal/locprov"

type IntType struct {
	*locprov.Location
}

type FloatType struct {
	*locprov.Location
}

type StringType struct {
	*locprov.Location
}

type BoolType struct {
	*locprov.Location
}

type AnyType struct {
	*locprov.Location
}

type NullableType struct {
	Type any

	*locprov.Location
}

type CustomType struct {
	Name string

	*locprov.Location
}

type ObjectType struct {
	Fields []any

	*locprov.Location
}

type Extends struct {
	Name string

	*locprov.Location
}

type ArrayType struct {
	Type any

	*locprov.Location
}

type MapType struct {
	Type any

	*locprov.Location
}

type EnumType struct {
	Values []EnumValue

	*locprov.Location
}

type EnumValue struct {
	Name  string
	Value string

	*locprov.Location
}

type ExternalType struct {
	ID string

	*locprov.Location
}

package ast

import "gitlab.com/akabio/ripgen/internal/locprov"

type AST struct {
	Name             string
	Mutations        []*Mutation
	Queries          []*Query
	Streams          []*Stream
	TypeDeclarations []*TypeDeclaration
}

type DynamicType struct {
	JSON       string
	JavaScript string
	Go         string

	*locprov.Location
}

type TypeDeclaration struct {
	Name    string
	Comment []string
	Type    any

	*locprov.Location
}

type Mutation struct {
	Name       string
	Comment    []string
	Parameters []*NamedType
	Response   any

	*locprov.Location
}

type Query struct {
	Name       string
	Comment    []string
	Parameters []*NamedType
	Response   any

	*locprov.Location
}

type Stream struct {
	Name       string
	Comment    []string
	Parameters []*NamedType

	*locprov.Location
}

type NamedType struct {
	Name    string
	Type    any
	Comment string

	*locprov.Location
}

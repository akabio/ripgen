package reader

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/akabio/ripgen/internal/ast"
	"gitlab.com/akabio/ripgen/internal/locprov"
	"gitlab.com/akabio/ripgen/model"
)

type reader struct {
	src         *ast.AST
	dest        *model.Model
	errors      []error
	customTypes map[string]*model.CustomType
	model       *model.Model
}

func Read(a *ast.AST) (*model.Model, []error) {
	m := &model.Model{
		Name: a.Name,
	}

	r := &reader{
		src:         a,
		dest:        m,
		customTypes: map[string]*model.CustomType{},
		model:       m,
	}

	// add and flatten type declarations
	for _, dec := range a.TypeDeclarations {
		m.TypeDeclarations = append(m.TypeDeclarations, r.readTypeDeclaration(dec))
	}

	// add mutations
	for _, c := range a.Mutations {
		mut := &model.Mutation{
			Name:       c.Name,
			Comment:    c.Comment,
			Parameters: r.readParameterList(c.Parameters),
			Response:   r.mapAndAddCallDeclaration(c.Name+"Response", c.Response),
		}
		m.Mutations = append(m.Mutations, mut)
	}

	// add queries
	for _, c := range a.Queries {
		q := &model.Query{
			Name:       c.Name,
			Comment:    c.Comment,
			Parameters: r.readParameterList(c.Parameters),
			Response:   r.mapAndAddCallDeclaration(c.Name+"Response", c.Response),
		}
		m.Queries = append(m.Queries, q)
	}

	// add streams
	for _, s := range a.Streams {
		s := &model.Stream{
			Name:       s.Name,
			Comment:    s.Comment,
			Parameters: r.readParameterList(s.Parameters),
		}
		m.Streams = append(m.Streams, s)
	}

	return m, r.errors
}

func (r *reader) readParameterList(params []*ast.NamedType) []*model.NamedType {
	ps := []*model.NamedType{}
	for _, p := range params {
		ps = append(ps, r.readNamedType(p))
	}

	return ps
}

func (r *reader) readNamedType(nt *ast.NamedType) *model.NamedType {
	return &model.NamedType{
		Name: nt.Name,
		Type: r.readType(nt.Name, nt.Type),
	}
}

func (r *reader) mapAndAddCallDeclaration(name string, in interface{}) model.Type {
	if in == nil {
		return nil
	}

	if enum, is := in.(*ast.EnumType); is {
		r.dest.TypeDeclarations = append(r.dest.TypeDeclarations, &model.TypeDeclaration{
			Name: name,
			Type: r.readType(name, enum),
		})

		return &model.CustomType{
			Name: name,
			Type: r.readType(name, enum),
		}
	}

	if obj, is := in.(*ast.ObjectType); is {
		r.dest.TypeDeclarations = append(r.dest.TypeDeclarations, &model.TypeDeclaration{
			Name: name,
			Type: r.readType(name, obj),
		})

		return &model.CustomType{
			Name: name,
			Type: r.readType(name, obj),
		}
	}

	if obj, is := in.(*ast.NullableType); is {
		return &model.NullableType{
			Type: r.mapAndAddCallDeclaration(name, obj.Type),
		}
	}

	return r.readType(name, in)
}

func (r *reader) readTypeDeclaration(at *ast.TypeDeclaration) *model.TypeDeclaration {
	return &model.TypeDeclaration{
		Name:    at.Name,
		Comment: at.Comment,
		Type:    r.readType(at.Name, at.Type),
	}
}

func (r *reader) readType(name string, aType any) model.Type {
	ct, location := r.readTypeImpl(name, aType)

	if ct != nil {
		r.model.AddHoverFunc(location, ct.Describe)
	}

	return ct
}

func (r *reader) readTypeImpl(name string, aType any) (model.Type, *locprov.Location) {
	switch t := aType.(type) {
	case *ast.IntType:
		return &model.IntType{}, t.Location
	case *ast.EnumType:
		desc := ""
		for _, ev := range t.Values {
			desc += "  - " + ev.Name + "\n"
		}

		return &model.EnumType{
			Values: r.readEnumValues(t.Values),
		}, t.Location
	case *ast.StringType:
		return &model.StringType{}, t.Location
	case *ast.BoolType:
		return &model.BoolType{}, t.Location
	case *ast.FloatType:
		return &model.FloatType{}, t.Location
	case *ast.ExternalType:
		return &model.ExternalType{ID: t.ID}, t.Location
	case *ast.ObjectType:
		return r.readObjectType(name, t), t.Location
	case *ast.ArrayType:
		return &model.ArrayType{
			Type: r.readType(name, t.Type),
		}, t.Location
	case *ast.MapType:
		return &model.MapType{
			Type: r.readType(name, t.Type),
		}, t.Location
	case *ast.AnyType:
		return &model.AnyType{}, t.Location
	case *ast.NullableType:
		return &model.NullableType{
			Type: r.readType(name, t.Type),
		}, t.Location
	case *ast.CustomType:
		tt, err := r.getTypeByName(t.Name, t.Location)
		if err != nil {
			r.addErr(err)
			return nil, nil
		}

		// if we have a reference to an external type we replace
		// the reference by the external type
		ext, isExt := tt.(*ast.ExternalType)
		if isExt {
			return &model.ExternalType{
				ID: ext.ID,
			}, t.Location
		}

		ct, has := r.customTypes[t.Name]
		if has { // if we have already one that is being built we use that (needed for recursive types)
			return ct, t.Location
		}

		ct = &model.CustomType{
			Name: t.Name,
		}
		r.customTypes[t.Name] = ct

		// read type after adding customType to customType map to prevent recursion
		ct.Type = r.readType(t.Name, tt)

		return ct, t.Location

	case nil:
		// there must have been an error down the line which is already reported
		return nil, nil
	}

	r.addErr(fmt.Errorf("unknown ast type %T", aType))

	return nil, nil
}

func (r *reader) getTypeByName(name string, location *locprov.Location) (any, error) {
	for _, td := range r.src.TypeDeclarations {
		if td.Name == name {
			return td.Type, nil
		}
	}

	return nil, &model.UnknownTypeError{Name: name, Location: location}
}

func (r *reader) readObjectType(name string, ot *ast.ObjectType) *model.ObjectType {
	return &model.ObjectType{
		Fields: r.readObjectFields([]string{}, name, ot).slice(),
	}
}

func (r *reader) addErr(err error) {
	if err != nil {
		r.errors = append(r.errors, err)
	}
}

type namedTypes struct {
	entity string
	nts    []*model.NamedType
}

func (nt *namedTypes) slice() []*model.NamedType {
	return append([]*model.NamedType{}, nt.nts...)
}

func (nt *namedTypes) push(name string, t model.Type, comment string) error {
	for _, nf := range nt.nts {
		if nf.Name == name {
			return fmt.Errorf("field %v.%v is already declared", nt.entity, name)
		}
	}

	nt.nts = append(nt.nts, &model.NamedType{
		Name:    name,
		Type:    t,
		Comment: comment,
	})

	return nil
}

func (r *reader) readObjectFields(read []string, name string, ot *ast.ObjectType) *namedTypes {
	for _, rot := range read {
		if rot == name {
			r.addErr(fmt.Errorf("circular extension %v", strings.Join(read, "->")))

			return &namedTypes{entity: name}
		}
	}

	nts := &namedTypes{entity: name}

	for _, f := range ot.Fields {
		switch ft := f.(type) {
		case *ast.NamedType:
			err := nts.push(ft.Name, r.readType(ft.Name, ft.Type), ft.Comment)
			if err != nil {
				r.addErr(err)
			}
		case *ast.Extends:
			stp, err := r.getTypeByName(ft.Name, ft.Location)
			if err != nil {
				r.addErr(err)
				continue
			}

			ot, isObjType := stp.(*ast.ObjectType)
			if !isObjType {
				log.Printf("can not extend non object type '%v'", ft.Name)
			}

			tsToAdd := r.readObjectFields(append(append([]string{}, read...), name), ft.Name, ot)
			for _, tst := range tsToAdd.nts {
				err := nts.push(tst.Name, tst.Type, tst.Comment)
				if err != nil {
					r.addErr(err)
				}
			}
		}
	}

	return nts
}

func (r *reader) readEnumValues(evs []ast.EnumValue) []model.EnumValue {
	ret := []model.EnumValue{}
	for _, ev := range evs {
		ret = append(ret, model.EnumValue{Name: ev.Name, Value: ev.Value})
	}

	return ret
}

package reader

import (
	"testing"

	"github.com/akabio/expect"
)

type htc struct {
	M string
	L int
	C int
}

var htcs = map[string]htc{
	"int type": {
		L: 2, C: 4, M: "int",
	},
	"string type": {
		L: 3, C: 4, M: "string",
	},
	"float type": {
		L: 4, C: 4, M: "float",
	},
	"bool type": {
		L: 5, C: 4, M: "bool",
	},
}

var source = `
type Attributes {
  i int
  s string
  f float
  b bool
}

`

func TestHover(t *testing.T) {
	m, err := Parse([]Source{
		{
			Source: []byte(source),
			Name:   "foo",
		},
	})
	if err != nil {
		t.Fatal(err)
	}

	for name, tc := range htcs {
		tc := tc

		t.Run(name, func(t *testing.T) {
			desc := m.FindHover(tc.L, tc.C)

			expect.Value(t, "hover", desc).ToBe(tc.M)
		})
	}
}

package reader

import (
	"testing"
)

type etc struct {
	S string
	E []string
}

var etcs = map[string]etc{
	"unknown type": {
		S: "type RecA { b RecB }",
		E: []string{"test.rip 0/14: unknown type RecB"},
	},
	"unknown type in extends": {
		S: "type RecA { ...RecB }",
		E: []string{"test.rip 0/15: unknown type RecB"},
	},
	"unknown type in parameters": {
		S: "query foo(a RecB): int",
		E: []string{"test.rip 0/12: unknown type RecB"},
	},
	"mismatched input": {
		S: "type RecA []",
		E: []string{"test.rip 0/11: mismatched input ']' expecting {'int', 'float', 'string', 'bool', 'any', '[', '{', TYPE_IDENTIFIER}"},
	},
}

func TestErrors(t *testing.T) {
	for name, tc := range etcs {
		tc := tc

		t.Run(name, func(t *testing.T) {
			_, errs := ParseDetailed([]Source{{
				Source: []byte(tc.S),
				Name:   "test.rip",
			}})

			expectedErr := map[string]int{}
			for _, e := range tc.E {
				expectedErr[e]++
			}

			for _, e := range errs {
				expectedErr[e.Error()]--
			}

			for err, sum := range expectedErr {
				if sum > 0 {
					t.Errorf("Expected error `%v` but did not find it", err)
				}
				if sum < 0 {
					t.Errorf("Unexpected error `%v`", err)
				}
			}
		})
	}
}

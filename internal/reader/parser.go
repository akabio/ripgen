package reader

import (
	"fmt"
	"os"

	"github.com/antlr4-go/antlr/v4"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/akabio/ripgen/internal/ast"
	"gitlab.com/akabio/ripgen/internal/parser"
	"gitlab.com/akabio/ripgen/model"
)

func Parse(sources []Source) (*model.Model, error) {
	m, errs := ParseDetailed(sources)

	var err error

	for _, e := range errs {
		fmt.Fprintln(os.Stderr, e)
		err = multierror.Append(err, e)
	}

	return m, err
}

func ParseDetailed(sources []Source) (*model.Model, []error) {
	visi := &parser.ASTVisitor{
		BaseRipgenVisitor: &parser.BaseRipgenVisitor{},
		AST:               &ast.AST{},
	}

	errors := []error{}

	for _, source := range sources {
		errs := parser.NewErrorListener(source.Name)

		input := antlr.NewInputStream(string(source.Source))
		lexer := parser.NewRipgenLexer(input)
		stream := antlr.NewCommonTokenStream(lexer, 0)

		p := parser.NewRipgenParser(stream)
		p.AddErrorListener(errs)
		p.BuildParseTrees = true
		tree := p.Start_()

		visi.Name = source.Name
		tree.Accept(visi)

		errors = append(errors, errs.Errors...)
	}

	mod, errs := Read(visi.AST)
	errors = append(errors, errs...)

	return mod, errors
}

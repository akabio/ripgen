package locprov

import "fmt"

type Location struct {
	file       string
	lineFrom   int
	lineTo     int
	columnFrom int
	columnTo   int
}

func New(file string, lf, lt, cf, ct int) *Location {
	return &Location{
		file:       file,
		lineFrom:   lf,
		lineTo:     lt,
		columnFrom: cf,
		columnTo:   ct,
	}
}

func (l *Location) Line() (int, int) {
	if l == nil {
		return 0, 0
	}

	return l.lineFrom, l.lineTo
}

func (l *Location) Column() (int, int) {
	if l == nil {
		return 0, 0
	}

	return l.columnFrom, l.columnTo
}

func (l *Location) File() string {
	if l == nil {
		return "unknown"
	}

	return l.file
}

func (l *Location) Describe() string {
	if l == nil {
		return "unknown location"
	}

	line, _ := l.Line()
	col, _ := l.Column()

	return fmt.Sprintf("%v %v/%v", l.file, line, col)
}

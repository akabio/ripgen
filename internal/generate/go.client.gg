import go(ipkg=ipkg, extpkg=extpkg, extTypes=extTypes)
import godecoder(ipkg=ipkg, extpkg=extpkg, extTypes=extTypes)
import goencoder(ipkg=ipkg, extpkg=extpkg, extTypes=extTypes)

main visitor:
model.Model type:
  // Code generated by gitlab.com/akabio/ripgen DO NOT EDIT.
  package ${cpkg}

  import (
    "bytes"
    "errors"
    "io"
    "io"
    "net/http"
  	jsoniter "github.com/json-iterator/go"
    "${ipkgImport}"
    {for i, v in extImports-}
    "${i}"
    {end}
  )

  type requestFactory func(method, url string, body io.Reader) (*http.Request, error)

  type client struct {
    c          *http.Client
    baseURL    string
    newRequest requestFactory
  }

  {if .Mutations}
  type mutation struct {
    c *client
  }
  {end}

  {if .Queries}
  type query struct {
    c *client
  }
  {end}

  {if .Streams}
  type stream struct {
    c *client
  }
  {end}

  func Create(baseURL string, c *http.Client, newRequest requestFactory) ${ipkg}.Calls {
    if c == nil {
      c = &http.Client{Timeout: time.Second * 5}
    }
    return &client{
      c:          c,
      baseURL:    baseURL,
      newRequest: newRequest,
    }
  }


  {if .Mutations}
  func (c *client)Mutation() ${ipkg}.Mutation {
    return &mutation {
      c: c,
    }
  }
  {end}

  {if .Queries}
  func (c *client)Query() ${ipkg}.Query {
    return &query {
      c: c,
    }
  }
  {end}

  {if .Streams}
  func (c *client)Stream() ${ipkg}.Stream {
    return &stream {
      c: c,
    }
  }
  {end}

  {for mutation in .Mutations-}
    @{mutation}
  {end}

  {for query in .Queries-}
    @{query}
  {end}

  {for stream in .Streams-}
    @{stream}
  {end}

  // json decoder methods
  @{godecoder.decoders: .}

  // json encoder methods
  @{goencoder.encoders: .}


model.Mutation type:
  func(ø *mutation)@{go.signature: .}{
    @{marshalParams: .}

    req, err := ø.c.newRequest("POST", ø.c.baseURL + "/mutation/${.Name | asId}", {if .Parameters}paramReader{else}nil{end})
    if err != nil {
      return @{returnErr: .}
    }

    {if .Response}
      resp, err := ø.c.c.Do(req)
      if err != nil {
        return @{returnErr: .}
      }
      bin, err := io.ReadAll(resp.Body)
      if err != nil {
        return @{returnErr: .}
      }
      if resp.StatusCode != http.StatusOK {
        err = errors.New(string(bin))
        return @{returnErr: .}
      }


      result := @{go.zeroType: .Response}

      it := jsoniter.ParseBytes(jsoniter.ConfigDefault, bin)

      @{godecoder.decode: .Response var="result"}

      if err != nil {
        return @{returnErr: .}
      }
      return result, nil
    {else}
      resp, err := ø.c.c.Do(req)
      if err != nil {
        return @{returnErr: .}
      }
      
      if resp.StatusCode != http.StatusOK {
        bin, err := io.ReadAll(resp.Body)
        if err != nil {
          return @{returnErr: .}
        }
        err = errors.New(string(bin))
        return @{returnErr: .}
      }

      return err
    {end}
  }

model.Query type:
  func(ø *query)@{go.signature: .}{
    @{marshalParams: .}

    req, err := ø.c.newRequest("POST", ø.c.baseURL + "/query/${.Name | asId}", {if .Parameters}paramReader{else}nil{end})
    if err != nil {
      return @{returnErr: .}
    }

    resp, err := ø.c.c.Do(req)
    if err != nil {
      return @{returnErr: .}
    }
    bin, err := io.ReadAll(resp.Body)
    if err != nil {
      return @{returnErr: .}
    }

    if resp.StatusCode != http.StatusOK {
      err = errors.New(string(bin))
      return @{returnErr: .}
    }

    result := @{go.zeroType: .Response}

    it := jsoniter.ParseBytes(jsoniter.ConfigDefault, bin)

    @{godecoder.decode: .Response var="result"}

    if err != nil {
      return @{returnErr: .}
    }
    return result, nil
  }


model.Stream type:
  func(ø *stream)@{go.signature: .}{
    @{marshalParams: .}

    req, err := ø.c.newRequest("POST", ø.c.baseURL + "/stream/${.Name | asId}", {if .Parameters}paramReader{else}nil{end})
    if err != nil {
      return @{returnErr: .}
    }

    resp, err := ø.c.c.Do(req)
    if err != nil {
      return err
    }

    if resp.StatusCode != http.StatusOK {
      bin, errs := io.ReadAll(resp.Body)
      if errs != nil {
        err = errs
      }
      err = errors.New(string(bin))
      return @{returnErr: .}
    }

    buf := make([]byte, 1024)
    for {
      n, err := resp.Body.Read(buf)
      if n > 0 {
        _, err := w.Write(buf[:n])
        if err != nil {
          return err
        }
      }

      if err != nil {
        if err == io.EOF {
          return nil
        }
        return err
      }
    }
  }


model.NamedType type:
  ${.Name | asID} @{go.type: .Type}


marshalParams visitor:
* type:
  {if .Parameters}
    jStream := jsoniter.NewStream(jsoniter.ConfigDefault, nil, 10000)
    err := func() error {
      jStream.WriteObjectStart()
      {for i, p, first in .Parameters-}
        {if !first}
          jStream.WriteMore()
        {end}
        jStream.WriteObjectField("${p.Name}")
        @{goencoder.encode: p.Type var="ω" + p.Name}
      {end-}

      jStream.WriteObjectEnd()
      return nil
    }()

    if err != nil {
      return @{returnErr: .}
    }

    paramReader := bytes.NewReader(jStream.Buffer())
  {end}


returnErr visitor:
model.Stream type:
  err

* type:
  {if .Response}@{go.zeroType: .Response}, {end-}
  err

parseResponse visitor:
<nil> type:


encode visitor:
model.StringType type:
  url.PathEscape(${val|asID})
* type:
  ${val|asID}


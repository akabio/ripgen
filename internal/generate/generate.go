package generate

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/akabio/fmtid"
	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/iotool"
	"gitlab.com/akabio/ripgen/model"
	"golang.org/x/tools/imports"
)

func Generate(mod *model.Model, destFile, template string, vars map[string]interface{}, idFormats *fmtid.Formatter) error {
	_, filename, _, _ := runtime.Caller(0)
	dir := filepath.Dir(filename)

	data, err := os.ReadFile(filepath.Join(dir, template))
	if err != nil {
		return err
	}

	templateDir := filepath.Dir(filepath.Join(dir, template))

	gg, err := gogen.Parse(string(data), template, includeFrom(templateDir))
	if err != nil {
		return err
	}

	opts := []gogen.Option{}
	for k, v := range idFormats.Formats {
		opts = append(opts, gogen.FilterOption(k, v))
	}

	opts = append(opts, gogen.FilterOption("kebab", idFormats.Formats["as-id"]))

	for v, val := range vars {
		opts = append(opts, gogen.WithVar(v, val))
	}

	opts = append(opts, externalFilter(vars))

	result, err := gogen.Execute(gg, mod, opts...)
	if err != nil {
		return err
	}

	if strings.HasSuffix(destFile, ".go") {
		result = goImports(destFile, result)
	}

	return iotool.WriteFileIfChanged(destFile, []byte(result), 0o600)
}

func externalFilter(vars map[string]interface{}) gogen.Option {
	externals := map[string]string{}

	exti, has := vars["externals"]
	if has {
		externals = exti.(map[string]string)
	}

	return gogen.FilterOption("external", func(name string) (string, error) {
		val, has := externals[name]
		if !has {
			return "", fmt.Errorf("please provide typescript mapping for external '%v'", name)
		}

		return val, nil
	})
}

func includeFrom(dir string) func(string) (string, string, error) {
	return func(n string) (string, string, error) {
		source := filepath.Join(dir, n+".gg")

		d, err := os.ReadFile(source)
		if err != nil {
			return "", "", err
		}

		return string(d), source, nil
	}
}

// goImports takes the source and formats it and updates imports.
func goImports(file, source string) string {
	result, err := imports.Process(file, []byte(source), &imports.Options{Comments: true})
	if err != nil {
		return "//  goimports failed, there are errors!\n" + source
	}

	return string(result)
}

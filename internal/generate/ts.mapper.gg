import ts

main visitor:
model.Model type:
  {for imp in imports}
  ${imp}{end}
  import * as types from './types.gen'

  // @ts-ignore in case it's unused
  function mapObject<FROM, TO>(
    m: { [key: string]: FROM },
    fn: (this: void, v: FROM) => TO
  ): { [key: string]: TO } {
    const ob: { [key: string]: TO } = {};
    Object.entries(m).forEach((item) => {
      ob[item[0]] = fn(item[1]);
    });
    return ob;
  }

  export type Encoders@{ts.genericMapping: .} = {
    {for name, type in .DynamicTypes-}
    ${name|asId}: (d: JS_${name|ASID}) => string
    {end}
  }

  export type Encoder@{ts.genericMapping: .} = {
    {for query in .Queries-}
    {-if query.Parameters}
    ${query.Name | asId}Parameters(params: @{paramType: query.Parameters generic="JavaScript"}) : @{paramType: query.Parameters generic="JSON"}
    {end}
    {-if query.Response}
    ${query.Name | asId}Response(resp: @{ts.type: query.Response ns="types", generic="JavaScript"}) : @{ts.type: query.Response ns="types", generic="JSON"}
    {-end}
    {end}

    {for mut in .Mutations-}
    {-if mut.Parameters}
    ${mut.Name | asId}Parameters(params: @{paramType: mut.Parameters generic="JavaScript"}) : @{paramType: mut.Parameters generic="JSON"}
    {end}
    {-if mut.Response}
    ${mut.Name | asId}Response(resp: @{ts.type: mut.Response ns="types", generic="JavaScript"}) : @{ts.type: mut.Response ns="types", generic="JSON"}
    {-end}
    {end}

    {for str in .Streams-}
    {-if str.Parameters}
    ${str.Name | asId}Parameters(params: @{paramType: str.Parameters generic="JavaScript"}) : @{paramType: str.Parameters generic="JSON"}
    {end}
    {end}

  }


  export type Decoders@{ts.genericMapping: .} = {
    {for name, type in .DynamicTypes-}
    ${name|asId}: (s: string) => JS_${name|ASID}
    {end}
  }

  export type Decoder@{ts.genericMapping: .} = {
    {for query in .Queries-}
    {-if query.Parameters}
    ${query.Name | asId}Parameters(params: @{paramType: query.Parameters generic="JSON"}) : @{paramType: query.Parameters generic="JavaScript"}
    {end}
    {-if query.Response}
    ${query.Name | asId}Response(resp: @{ts.type: query.Response ns="types", generic="JSON"}) : @{ts.type: query.Response ns="types", generic="JavaScript"}
    {-end}
    {end}

    {for mut in .Mutations-}
    {-if mut.Parameters}
    ${mut.Name | asId}Parameters(params: @{paramType: mut.Parameters generic="JSON"}) : @{paramType: mut.Parameters generic="JavaScript"}
    {end}
    {-if mut.Response}
    ${mut.Name | asId}Response(resp: @{ts.type: mut.Response ns="types", generic="JSON"}) : @{ts.type: mut.Response ns="types", generic="JavaScript"}
    {-end}
    {end}

    {for str in .Streams-}
    {-if str.Parameters}
    ${str.Name | asId}Parameters(params: @{paramType: str.Parameters generic="JSON"}) : @{paramType: str.Parameters generic="JavaScript"}
    {end}
    {end}

  }

  export function encoder@{ts.genericMapping: .}({if .DynamicTypes | len > 0}encode: Encoders@{ts.useGeneric: .}{end}): Encoder@{ts.useGeneric: .}{
    {for type in .TypeDeclarations-}
      {if type.Type | type == "*model.ObjectType"-}
    {indent}// @ts-ignore in case it's unused
    const toWire${type.Name | AsId} = (p: @{ts.type: type.Type ns="types", generic="JavaScript"-}
    ):@{ts.type: type.Type ns="types", generic="JSON"-} => {
    return @{toWire: type.Type s="p"}
  }{end}
      {-end}
    {end}

    return {
      {for query in .Queries-}
      {indent}@{encoder: query}{end}
      {end}
      {for mut in .Mutations-}
      {indent}@{encoder: mut}{end}
      {end}
      {for str in .Streams-}
      {indent}@{encoder: str}{end}
      {end}
    }
  }

  export function decoder@{ts.genericMapping: .}({if .DynamicTypes | len > 0}decode: Decoders@{ts.useGeneric: .}{end}): Decoder@{ts.useGeneric: .}{
    {for type in .TypeDeclarations-}
      {if type.Type | type == "*model.ObjectType"-}
    {indent}// @ts-ignore in case it's unused
    const fromWire${type.Name | AsId} = (p: @{ts.type: type.Type ns="types", generic="JSON"-}
    ):@{ts.type: type.Type ns="types", generic="JavaScript"-} => {
    return @{fromWire: type.Type s="p"}
  }{end}
      {-end}
    {end}

    return {
      {for query in .Queries-}
      {indent}@{decoder: query}{end}
      {end}
      {for mut in .Mutations-}
      {indent}@{decoder: mut}{end}
      {end}
      {for str in .Streams-}
      {indent}@{decoder: str}{end}
      {end}
    }
  }

paramType visitor:
[]model.NamedType type(generic=""):
  {{for p in .}
    ${p.Name}: @{ts.type: p.Type ns="types", generic=generic},
  {end}}
  

encoder visitor:
model.Query type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JavaScript"}) : @{paramType: .Parameters generic="JSON"} => {
    return @{toWire: .Parameters s="params"}
  },
  {end}
  ${.Name | asId}Response: (resp: @{ts.type: .Response ns="types", generic="JavaScript"}) : @{ts.type: .Response ns="types", generic="JSON"} => {
    return @{toWire: .Response s="resp"}
  },

model.Mutation type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JavaScript"}) : @{paramType: .Parameters generic="JSON"} => {
    return @{toWire: .Parameters s="params"}
  },
  {end}
  {if .Response}
  ${.Name | asId}Response: (resp: @{ts.type: .Response ns="types", generic="JavaScript"}) : @{ts.type: .Response ns="types", generic="JSON"} => {
    return @{toWire: .Response s="resp"}
  },
  {end}

model.Stream type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JavaScript"}) : @{paramType: .Parameters generic="JSON"} => {
    return @{toWire: .Parameters s="params"}
  },
  {end}


model.NamedType type:
  ${.Name | asId}: @{ts.type: .Type ns="types", generic="JavaScript"}


toWire visitor:
model.ObjectType type(s = "default"):
  {
    {for f in .Fields-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
[]model.NamedType type(s = "default"):
  {
    {for f in .-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
model.CustomType type:
  {if .Type | type == "*model.ObjectType"-}
  toWire${.Name | AsId}(${s})
  {-else-}
  @{.Type s = s}
  {-end}

model.NullableType type:
  (${s} === null) ? null : @{.Type s = s}
model.ArrayType type:
  ${s}.map((e: @{ts.type: .Type ns="types", generic="JavaScript"}) => {
    return @{.Type s="e"}
  })
model.MapType type:
  mapObject(${s}, (e: @{ts.type: .Type ns="types", generic="JavaScript"}) => {
    return @{.Type s="e"}
  })
model.ExternalType type:
  encode.${.ID | asId}(${s})
* type:
  ${s}


decoder visitor:
model.Query type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JSON"}) : @{paramType: .Parameters generic="JavaScript"} => {
    return @{fromWire: .Parameters s="params"}
  },
  {end}
  ${.Name | asId}Response: (resp: @{ts.type: .Response ns="types", generic="JSON"}) : @{ts.type: .Response ns="types", generic="JavaScript"} => {
    return @{fromWire: .Response s="resp"}
  },

model.Mutation type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JSON"}) : @{paramType: .Parameters generic="JavaScript"} => {
    return @{fromWire: .Parameters s="params"}
  },
  {end}
  {if .Response}
  ${.Name | asId}Response: (resp: @{ts.type: .Response ns="types", generic="JSON"}) : @{ts.type: .Response ns="types", generic="JavaScript"} => {
    return @{fromWire: .Response s="resp"}
  },
  {end}

model.Stream type:
  {if .Parameters}
  ${.Name | asId}Parameters: (params: @{paramType: .Parameters generic="JSON"}) : @{paramType: .Parameters generic="JavaScript"} => {
    return @{fromWire: .Parameters s="params"}
  },
  {end}

fromWire visitor:
model.ObjectType type(s = "default"):
  {
    {for f in .Fields-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
[]model.NamedType type(s = "default"):
  {
    {for f in .-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
model.CustomType type:
  {if .Type | type == "*model.ObjectType"-}
  fromWire${.Name | AsId}(${s})
  {-else-}
  @{.Type s = s}
  {-end}

model.NullableType type:
  (${s} === null) ? null : @{.Type s = s}
model.ArrayType type:
  ${s}.map((e: @{ts.type: .Type ns="types", generic="JSON"}) => {
    return @{.Type s="e"}
  })
model.MapType type:
  mapObject(${s}, (e: @{ts.type: .Type ns="types", generic="JSON"}) => {
    return @{.Type s="e"}
  })
model.ExternalType type:
  decode.${.ID | asId}(${s})
* type:
  ${s}
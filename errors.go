package ripgen

import "fmt"

type BadRequestError struct {
	cause error
}

func NewBadRequest(err error) error {
	if err == nil {
		return nil
	}

	return &BadRequestError{cause: err}
}

func (e *BadRequestError) Error() string {
	return fmt.Sprintf("bad request, %v", e.cause)
}

// Unwrap allows for go1.13+ errors.Unwrap(...), errors.Is(...) and errors.As(...)
func (e *BadRequestError) Unwrap() error {
	return e.cause
}

// Cause allows for github.com/pkg/errors.Cause(...)
func (e *BadRequestError) Cause() error {
	return e.cause
}

package ripgen

type Option interface {
	apply(generator *Generator) error
}

type acronym string

func OptionAcronym(a string) Option {
	return acronym(a)
}

func (a acronym) apply(g *Generator) error {
	g.formatter.Acronyms[string(a)] = true
	return nil
}

type acronymPlural string

func OptionAcronymPlural(a string) Option {
	return acronymPlural(a)
}

func (a acronymPlural) apply(g *Generator) error {
	g.formatter.PluralAcronyms[string(a)] = true
	return nil
}

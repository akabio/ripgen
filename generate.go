package ripgen

import (
	"os"
	"path"
	"path/filepath"

	"gitlab.com/akabio/ripgen/internal/generate"
	"gitlab.com/akabio/ripgen/internal/reader"
	"gitlab.com/akabio/ripgen/model"
)

type Source struct {
	Source []byte
	Name   string
}

func (g *Generator) Debug() ([]byte, error) {
	tmpf, err := os.MkdirTemp("", "ripgentest")
	if err != nil {
		return nil, err
	}

	defer os.RemoveAll(tmpf)

	file := path.Join(tmpf, "debugg.txt")

	err = generate.Generate(g.ast, file, "debug.gg", map[string]interface{}{}, g.formatter)
	if err != nil {
		return nil, err
	}

	return os.ReadFile(file)
}

type TSParams struct {
	Externals            map[string]string
	Imports              []string
	SingleParamMutations bool
}

func (g *Generator) TypescriptTypes(targetFile string, params TSParams) error {
	tsFolder := filepath.Dir(targetFile)

	err := os.MkdirAll(tsFolder, 0o700)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, targetFile, "ts.types.gg", map[string]interface{}{
		"externals":            params.Externals,
		"imports":              params.Imports,
		"singleParamMutations": params.SingleParamMutations,
	}, g.formatter)
}

func (g *Generator) TypescriptClient(targetFile string, params TSParams) error {
	tsFolder := filepath.Dir(targetFile)

	err := os.MkdirAll(tsFolder, 0o700)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, targetFile, "ts.client.gg", map[string]interface{}{
		"externals":            params.Externals,
		"imports":              params.Imports,
		"singleParamMutations": params.SingleParamMutations,
	}, g.formatter)
}

func (g *Generator) TypescriptMapper(targetFile string, params TSParams) error {
	tsFolder := filepath.Dir(targetFile)

	err := os.MkdirAll(tsFolder, 0o700)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, targetFile, "ts.mapper.gg", map[string]interface{}{
		"externals":            params.Externals,
		"imports":              params.Imports,
		"singleParamMutations": params.SingleParamMutations,
	}, g.formatter)
}

func (g *Generator) TanstackVueQuery(targetFile string, params TSParams) error {
	tsFolder := filepath.Dir(targetFile)

	err := os.MkdirAll(tsFolder, 0o700)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, targetFile, "ts.vquery.gg", map[string]interface{}{
		"externals":            params.Externals,
		"imports":              params.Imports,
		"singleParamMutations": params.SingleParamMutations,
		"moduleName":           g.moduleName,
	}, g.formatter)
}

func ms(source []Source) []reader.Source {
	gSources := []reader.Source{}
	for _, s := range source {
		gSources = append(gSources, reader.Source{Source: s.Source, Name: s.Name})
	}

	return gSources
}

func ParseDetailed(sources []Source) (*model.Model, []error) {
	return reader.ParseDetailed(ms(sources))
}

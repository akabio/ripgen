package runtime

type Endpoint struct {
	Action Action
	Name   string
}

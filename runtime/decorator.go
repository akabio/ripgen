package runtime

type Action string

var (
	Query    Action = "query"
	Mutation Action = "mutation"
	Stream   Action = "stream"
)

type DecoratorContext struct {
	Name   string
	Action Action
}

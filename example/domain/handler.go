package domain

import "gitlab.com/akabio/ripgen/example/api"

type handler struct{}

type query struct{}

func New() api.Calls {
	return &handler{}
}

func (h *handler) Query() api.Query {
	return &query{}
}

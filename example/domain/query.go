package domain

import (
	"time"

	"gitlab.com/akabio/ripgen/example/api"
	"gitlab.com/akabio/ripgen/example/external"
)

func (q *query) GetTask(id external.ID) (*api.Task, error) {
	return &api.Task{
		ID:      id,
		Title:   "Seven",
		Due:     time.Now().Add(time.Hour * 100),
		Created: time.Now(),
	}, nil
}

func (q *query) ApiVersion(string, api.IDThing) (int, error) {
	return 0, nil
}

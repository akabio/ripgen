package main

//go:generate bash -c "cd ../.. ; go run ./cmd/ripgen"

import (
	"log"
	"os"
	"path"

	"gitlab.com/akabio/ripgen"
)

func main() {
	dataSrc, err := os.ReadFile("example.rip")
	if err != nil {
		log.Fatal(err)
	}

	rootPkg := "gitlab.com/akabio/ripgen/example"

	gen, err := ripgen.NewGenerator([]ripgen.Source{
		{
			Source: dataSrc,
			Name:   "example.rip",
		},
	}, "example")
	if err != nil {
		log.Fatal(err)
	}

	gg := gen.Go(
		path.Join(rootPkg, "api"),
		path.Join(rootPkg, "external"),
		map[string]string{
			"Time": "time.Time",
			"Date": "time.Time",
			"Id":   "gitlab.com/akabio/ripgen/example/external.ID",
		},
	)

	// generate api interface and DTO into ./api package
	err = gg.Interface("api")
	if err != nil {
		log.Fatal(err)
	}

	// generate http handler into ./infrastructure/httpserver package
	err = gg.Server("infrastructure/httpserver")
	if err != nil {
		log.Fatal(err)
	}
}

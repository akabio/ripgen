package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/example/api"
	"gitlab.com/akabio/ripgen/example/domain"
	"gitlab.com/akabio/ripgen/example/infrastructure/httpserver"
)

func main() {
	r := httpserver.CreateRouter(func(w http.ResponseWriter, r *http.Request, p httprouter.Params) (api.Calls, error) {
		return domain.New(), nil
	}, ripgen.DefaultErrorHandler)

	fmt.Println("listen on :8001")
	log.Println(http.ListenAndServe(":8001", r))
}

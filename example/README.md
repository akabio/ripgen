Example project
===============

Run
---

    # generate api and infrastructure code
    go generate ./cmd/ripgen/

    # run server
    go run ./cmd/server/

    # test endpoint
    curl -X POST http://localhost:8001/query/getTask -d '{"id": "5"}'

Overview
--------

### ./example.rip

Interface declaration

### ./api

Package for generated api interfaces including DTO.

### ./domain

Package containing the implementations for the api endpoints.

### ./external

Package containing serializers/parsers for external datatypes used in the declaration.

### ./cmd/ripgen/main.go

Generates code from the declaration file. Using code instead of a cli allows for pinning the generator version using go.mod.

### ./cmd/server/main.go

Starts a http server using generated handler.

### ./infrastructure/httpserver

Package containing http handler.
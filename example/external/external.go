package external

import (
	"fmt"
	"strconv"
	"time"
)

func SerializeTime(t time.Time) (string, error) {
	return t.Format(time.RFC3339), nil
}

func ParseTime(t string) (time.Time, error) {
	return time.Parse(time.RFC3339, t)
}

func ZeroTime() time.Time {
	return time.Time{}
}

func SerializeDate(t time.Time) (string, error) {
	return t.Format("2006-01-02"), nil
}

func ParseDate(t string) (time.Time, error) {
	return time.Parse("2006-01-02", t)
}

func ZeroDate() time.Time {
	return time.Time{}
}

type ID int

func SerializeID(id ID) (string, error) {
	return fmt.Sprint(id), nil
}

func ParseID(t string) (ID, error) {
	i, err := strconv.Atoi(t)
	if err != nil {
		return ID(0), err
	}

	return ID(i), nil
}

func ZeroID() ID {
	return ID(0)
}

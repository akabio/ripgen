package ripgen

import (
	"errors"
	"testing"

	"github.com/akabio/expect"
	"github.com/hashicorp/go-multierror"
)

func init() {
	expect.Default.Output = expect.ColoredDiffOutput
}

func TestOrderOfExtends(t *testing.T) {
	r, _ := render(t, `
type A {
  a int
}
type B {
  ...A
  b int
}
type C {
  c int
  ...B
}
`)
	r.ToBe(`
type A {
  a int
}

type B {
  a int
  b int
}

type C {
  c int
  a int
  b int
}
`)
}

func TestExtendsRecursiveFails(t *testing.T) {
	_, e := render(t, `
type A {
  a int
  ...B
}
type B {
  ...A
  b int
}`)
	e.ToCount(2)
	e.First().Message().ToBe("circular extension A->B")
	e.Last().Message().ToBe("circular extension B->A")
}

func TestExtendsOverwriteNotAllowed(t *testing.T) {
	_, e := render(t, `
type A {
  a int
}
type B {
  a int
  ...A
}`)
	e.ToCount(1)
	e.First().Message().ToBe("field B.a is already declared")
}

func TestOverwriteNotAllowed(t *testing.T) {
	_, e := render(t, `
type A {
	a int
	a int
}
`)
	e.ToCount(1)
	e.First().Message().ToBe("field A.a is already declared")
}

func render(t *testing.T, tmpl string) (expect.Val, expect.Val) {
	t.Helper()

	gen, err := NewGenerator([]Source{{Source: []byte(tmpl)}}, "test")

	res := []byte{}
	if err == nil {
		res, err = gen.Debug()
	}

	expErrs := expect.Value(t, "errors", []error{})

	if err != nil {
		mes := &multierror.Error{}
		if !errors.As(err, &mes) {
			t.Errorf("failed to interpret returned error")
		}

		expErrs = expect.Value(t, "errors", mes.Errors)
	}

	return expect.Value(t, "result", string(res)), expErrs
}

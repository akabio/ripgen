package ripgen

import (
	"os"
	"path"
	"path/filepath"
	"strings"
	"unicode"

	"gitlab.com/akabio/fmtid"
	"gitlab.com/akabio/ripgen/internal/generate"
	"gitlab.com/akabio/ripgen/internal/reader"
	"gitlab.com/akabio/ripgen/model"
)

type GoGenerator struct {
	// InterfacePkg is the full package name where the interface definition is generated to.
	InterfacePkg string
	// ExternalPkg is the full package name where the parse/serialize and construct methods
	// for the different external types are located.
	ExternalPkg string
	// Externals is the mapping of the external names to go data types.
	Externals map[string]string

	generator *Generator
}

type Generator struct {
	// A list of rip sources, code and filename.
	source     []Source
	moduleName string
	ast        *model.Model
	formatter  *fmtid.Formatter
}

func NewGenerator(source []Source, moduleName string, options ...Option) (*Generator, error) {
	ast, err := reader.Parse(ms(source))
	if err != nil {
		return nil, err
	}

	gen := &Generator{
		source:     source,
		moduleName: moduleName,
		ast:        ast,
		formatter: fmtid.AddDefault(&fmtid.Formatter{
			Acronyms: map[string]bool{
				"id":   true,
				"http": true,
				"tcp":  true,
				"ip":   true,
			},
			PluralAcronyms: map[string]bool{
				"ids": true,
				"ips": true,
			},
			IsFirst: unicode.IsUpper,
			IsOther: func(r rune) bool {
				return unicode.IsLower(r) || unicode.IsNumber(r)
			},
		}),
	}

	for _, o := range options {
		err = o.apply(gen)
		if err != nil {
			return nil, err
		}
	}

	return gen, nil
}

func (g *Generator) Go(iPkg, extPkg string, externals map[string]string) *GoGenerator {
	return &GoGenerator{
		InterfacePkg: iPkg,
		ExternalPkg:  extPkg,
		Externals:    externals,
		generator:    g,
	}
}

func (g *GoGenerator) params() map[string]interface{} {
	d := map[string]interface{}{}

	d["ipkg"] = filepath.Base(g.InterfacePkg)
	d["ipkgImport"] = g.InterfacePkg

	extImports := map[string]bool{
		g.ExternalPkg: true,
	}

	extTypes := map[string]string{}

	for k, v := range g.Externals {
		imp := mkImport(v)
		if imp != "" {
			extImports[imp] = true
		}

		extTypes[k] = filepath.Base(v)
	}

	d["extTypes"] = extTypes
	d["extImports"] = extImports
	d["extpkg"] = filepath.Base(g.ExternalPkg)

	return d
}

func mkImport(i string) string {
	di := strings.LastIndex(i, ".")
	if di == -1 {
		return ""
	}

	return i[0:di]
}

func (g *GoGenerator) Interface(interfaceFolder string) error {
	err := os.MkdirAll(interfaceFolder, 0o700)
	if err != nil {
		return err
	}

	prms := g.params()

	err = generate.Generate(g.generator.ast, filepath.Join(interfaceFolder, "interface.gen.go"), "go.interface.gg", prms, g.generator.formatter)
	if err != nil {
		return err
	}

	err = generate.Generate(g.generator.ast, filepath.Join(interfaceFolder, "dto.gen.go"), "go.dto.gg", prms, g.generator.formatter)
	if err != nil {
		return err
	}

	err = generate.Generate(g.generator.ast, filepath.Join(interfaceFolder, "enum.gen.go"), "go.enum.gg", prms, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

func (g *GoGenerator) Server(handlerFolder string) error {
	err := os.MkdirAll(handlerFolder, 0o700)
	if err != nil {
		return err
	}

	vars := g.params()
	vars["hpkg"] = path.Base(handlerFolder)

	err = generate.Generate(g.generator.ast, filepath.Join(handlerFolder, "handler.gen.go"), "go.handler.gg", vars, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

func (g *GoGenerator) Profiler(profilerFolder string) error {
	err := os.MkdirAll(profilerFolder, 0o700)
	if err != nil {
		return err
	}

	vars := g.params()
	vars["hpkg"] = path.Base(profilerFolder)

	err = generate.Generate(g.generator.ast, filepath.Join(profilerFolder, "profiler.gen.go"), "go.profiler.gg", vars, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

func (g *GoGenerator) Decorator(decoratorFolder string) error {
	err := os.MkdirAll(decoratorFolder, 0o700)
	if err != nil {
		return err
	}

	vars := g.params()
	vars["hpkg"] = path.Base(decoratorFolder)

	err = generate.Generate(g.generator.ast, filepath.Join(decoratorFolder, "decorator.gen.go"), "go.decorator.gg", vars, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

func (g *GoGenerator) Client(clientFolder string) error {
	err := os.MkdirAll(clientFolder, 0o700)
	if err != nil {
		return err
	}

	vars := g.params()
	vars["cpkg"] = path.Base(clientFolder)

	err = generate.Generate(g.generator.ast, filepath.Join(clientFolder, "client.gen.go"), "go.client.gg", vars, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

func (g *GoGenerator) TestFacade(facadeFolder string) error {
	err := os.MkdirAll(facadeFolder, 0o700)
	if err != nil {
		return err
	}

	vars := g.params()
	vars["fpkg"] = path.Base(facadeFolder)

	err = generate.Generate(g.generator.ast, filepath.Join(facadeFolder, "testfacade.gen.go"), "go.testf.gg", vars, g.generator.formatter)
	if err != nil {
		return err
	}

	return nil
}

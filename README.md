# RIPGen

A RPC lib that mostly generates code.

Focus is on:
  - simplicity
  - private APIs (controlling both sides)
  - type checking over language barrier

Targets:
  - go interface: creates go interfaces used by other generators
  - go handler: creates a go server
  - go client: creates a go client
  - go decorator: decorate api calls, allows for: auhorisation, logging, transaction, profiling...
  - go testfacade: readable API tests
  - ts client: calls server side endpoints and maps data to js types

[Example project](./example/README.md)

## Declaration

The declaration consists of a list of query and mutation definitions and optionally data types.

Queries are calls that only read state and always return some data. They must not modify any state.

Mutations change state and should only return information belonging to that mutation.

    query getFooById(id int, count int, order string): [int]

The given declaration will generate Interfaces and code that allows it to call the defined
method remotely.

    GetFooByID(id int, count int, order string) ([]int, error) { ... }

A method with above signature will be generated for the client and this signature will be called for the server.

    
    async getFooById (id: number, count: number, order: string,): Promise<Array<number>> { ... }

Typescript client with above signature will be generated.

### DataTypes

#### Scalars

Scalars in the meaning of non compound types are:

- int
- float
- time
- bool
- enum
- string

The enum type has it's possible values defined in comma separated pairs of `name "value"`:

    `enum (default "", foo, bar, none "n")`

If no string value is given it's string representation equals to the name.

#### Arrays

Arrays are defined by surounding it's element type by square brackets:

    [string] // an array if strings

#### Maps

Maps keys are always of string type. They are defined by surounding it's value type by curly braces:

    {int} // a map of string->int

#### Objects

Objects have multiple named fields where each one can have it's own type. They are also defined using curly braces but they have multiple field name -> value pairs:

    {
      count  int
      name   string
      active bool
    }

Field names must always be lower case.

#### Nullables

Data types can be nullable, meaning that their possible values ad well as null or undefined is a valid state. The default is non nullable and nullability canbe marked by appending a questionmark.

    [string?]? // is an array of strings that can be null and can contain null values

Usually it's best to only allow null values when there is a good reason to do so.

#### Type declarations

Object types used as an Object property must be declared separately as a type:

    type Foo {
        name string
    }

    query getFoos(): {
        foos  [Foo]
        total int
    }

Custom types must always be uppercase, attributes lowercase.

### External types

    type Time external

Declares a type from the implementing language.
In Go it could be `time.Time` in Typescript `Date`. To send it over the network it needs to be serialized as a string.

### Doc comments

Comments are only allowed in front of type/call declarations or after object field declarations:
They will be written into the generated type declarations so IDEs provide doc tooltips.

    // Documentation for type
    type Foo {
        field string // Comment for field, only single line allowed
        age int      // The age of the foo
    }

    // will return all foos
    query getFoos(): [Foo]


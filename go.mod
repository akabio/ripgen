module gitlab.com/akabio/ripgen

go 1.19

require (
	github.com/akabio/expect v0.10.3
	github.com/antlr4-go/antlr/v4 v4.13.0
	github.com/hashicorp/go-multierror v1.1.1
	github.com/json-iterator/go v1.1.12
	github.com/julienschmidt/httprouter v1.3.0
	gitlab.com/akabio/expect v0.9.9
	gitlab.com/akabio/fmtid v0.2.2
	gitlab.com/akabio/gogen v0.6.1
	gitlab.com/akabio/iotool v0.5.4
	golang.org/x/tools v0.16.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sergi/go-diff v1.3.1 // indirect
	golang.org/x/exp v0.0.0-20231127185646-65229373498e // indirect
	golang.org/x/mod v0.14.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

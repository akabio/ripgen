package model

type Location interface {
	Line() (int, int)
	Column() (int, int)
	File() string
	Describe() string
}

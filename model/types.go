package model

import "fmt"

type IntType struct{}

func (t *IntType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *IntType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *IntType) Describe() string {
	return "int"
}

type FloatType struct{}

func (t *FloatType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *FloatType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *FloatType) Describe() string {
	return "float"
}

type StringType struct{}

func (t *StringType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *StringType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *StringType) Describe() string {
	return "string"
}

type BoolType struct{}

func (t *BoolType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *BoolType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *BoolType) Describe() string {
	return "bool"
}

type AnyType struct{}

func (t *AnyType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *AnyType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *AnyType) Describe() string {
	return "any"
}

type NullableType struct {
	Type Type
}

func (t *NullableType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return t.Type.dynamicTypes(visited)
}

func (t *NullableType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *NullableType) Describe() string {
	return t.Type.Describe() + "?"
}

func (t *NullableType) Nullable() bool {
	switch t.Type.(type) {
	case *ArrayType, *MapType, *AnyType:
		return true
	}

	return false
}

type CustomType struct {
	Name string
	Type Type
}

func (t *CustomType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	if visited[t] {
		return DynamicTypes{}
	}

	visited[t] = true

	return t.Type.dynamicTypes(visited)
}

func (t *CustomType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *CustomType) Describe() string {
	return t.Name + " = " + t.Type.Describe()
}

type ObjectType struct {
	Fields []*NamedType
}

func (t *ObjectType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	if visited[t] {
		return DynamicTypes{}
	}

	visited[t] = true

	m := DynamicTypes{}

	for _, f := range t.Fields {
		for k, v := range f.Type.dynamicTypes(visited) {
			m[k] = v
		}
	}

	return m
}

func (t *ObjectType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *ObjectType) Describe() string {
	return "object"
}

type ArrayType struct {
	Type Type
}

func (t *ArrayType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	if visited[t] {
		return DynamicTypes{}
	}

	visited[t] = true

	return t.Type.dynamicTypes(visited)
}

func (t *ArrayType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *ArrayType) Describe() string {
	return "array"
}

type MapType struct {
	Type Type
}

func (t *MapType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	if visited[t] {
		return DynamicTypes{}
	}

	visited[t] = true

	return t.Type.dynamicTypes(visited)
}

func (t *MapType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *MapType) Describe() string {
	return "map"
}

type EnumType struct {
	Values []EnumValue
}

func (t *EnumType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{}
}

func (t *EnumType) DynamicTypes() DynamicTypes {
	return t.dynamicTypes(map[interface{}]bool{})
}

func (t *EnumType) Describe() string {
	s := "enum\n"
	for _, val := range t.Values {
		s += fmt.Sprintf("  - %v\n", val.Name)
	}

	return s
}

type EnumValue struct {
	Name  string
	Value string
}

type ExternalType struct {
	ID string
}

func (e *ExternalType) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	return DynamicTypes{
		e.ID: {},
	}
}

func (e *ExternalType) DynamicTypes() DynamicTypes {
	return e.dynamicTypes(map[interface{}]bool{})
}

func (e *ExternalType) Describe() string {
	return "external"
}

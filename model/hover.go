package model

import (
	"gitlab.com/akabio/ripgen/internal/locprov"
)

type hover struct {
	from        int
	to          int
	description func() string
}

func (m *Model) AddHover(location *locprov.Location, desc string) {
	if m.hovers == nil {
		m.hovers = map[int][]hover{}
	}

	l, _ := location.Line()
	cf, ct := location.Column()

	m.hovers[l] = append(m.hovers[l], hover{
		from: cf,
		to:   ct,
		description: func() string {
			return desc
		},
	})
}

func (m *Model) AddHoverFunc(location *locprov.Location, description func() string) {
	if m.hovers == nil {
		m.hovers = map[int][]hover{}
	}

	l, _ := location.Line()
	cf, ct := location.Column()

	m.hovers[l] = append(m.hovers[l], hover{
		from:        cf,
		to:          ct,
		description: description,
	})
}

func (m *Model) FindHover(line, column int) string {
	if m.hovers == nil {
		return ""
	}

	for _, hover := range m.hovers[line] {
		if column >= hover.from && column <= hover.to {
			return hover.description()
		}
	}

	return ""
}

package model

import (
	"fmt"

	"gitlab.com/akabio/ripgen/internal/locprov"
)

type ParseError struct {
	*locprov.Location
	Message string
}

func (p *ParseError) Error() string {
	return fmt.Sprintf("%v: %v", p.Location.Describe(), p.Message)
}

type UnknownTypeError struct {
	*locprov.Location
	Name string
}

func (u *UnknownTypeError) Error() string {
	return fmt.Sprintf("%v: unknown type %v", u.Location.Describe(), u.Name)
}

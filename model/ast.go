package model

type Model struct {
	Name             string
	Mutations        []*Mutation
	Queries          []*Query
	Streams          []*Stream
	TypeDeclarations []*TypeDeclaration
	hovers           map[int][]hover
}

type Type interface {
	DynamicTypes() DynamicTypes
	Describe() string
	dynamicTypes(visited map[interface{}]bool) DynamicTypes
}

type DynamicType struct {
	// value of map, only key is relevant
}

type DynamicTypes map[string]DynamicType

type TypeDeclaration struct {
	Name    string
	Comment []string
	Type    Type
}

func (m *Model) GetTypeDeclaration(name string) *TypeDeclaration {
	for _, td := range m.TypeDeclarations {
		if td.Name == name {
			return td
		}
	}

	return nil
}

// DynamicTypes gets all the externals used in the current model.
func (m *Model) DynamicTypes() DynamicTypes {
	return m.dynamicTypes(map[interface{}]bool{})
}

func (m *Model) dynamicTypes(visited map[interface{}]bool) DynamicTypes {
	dTypes := map[string]DynamicType{}

	for _, c := range m.Queries {
		for _, param := range c.Parameters {
			for k, v := range param.Type.dynamicTypes(visited) {
				dTypes[k] = v
			}
		}

		for k, v := range c.Response.dynamicTypes(visited) {
			dTypes[k] = v
		}
	}

	for _, c := range m.Mutations {
		for _, param := range c.Parameters {
			for k, v := range param.Type.dynamicTypes(visited) {
				dTypes[k] = v
			}
		}

		if c.Response != nil {
			for k, v := range c.Response.dynamicTypes(visited) {
				dTypes[k] = v
			}
		}
	}

	for _, s := range m.Streams {
		for _, param := range s.Parameters {
			for k, v := range param.Type.dynamicTypes(visited) {
				dTypes[k] = v
			}
		}
	}

	return dTypes
}

type Mutation struct {
	Name       string
	Comment    []string
	Parameters []*NamedType
	Response   Type
}

type Query struct {
	Name       string
	Comment    []string
	Parameters []*NamedType
	Response   Type
}

type Stream struct {
	Name       string
	Comment    []string
	Parameters []*NamedType
}

type NamedType struct {
	Name    string
	Type    Type
	Comment string
}

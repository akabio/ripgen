package domain

import (
	"strconv"

	"gitlab.com/akabio/ripgen/test/body/calls"
)

type Domain struct{}

type mutation struct{}

type query struct{}

func (d *Domain) Mutation() calls.Mutation {
	return &mutation{}
}

func (d *Domain) Query() calls.Query {
	return &query{}
}

func (q *query) GetInt(body int) (int, error) {
	return body, nil
}

func (q *query) IntList(body []int) ([]int, error) {
	return body, nil
}

func (q *query) GetObjectResp(body int) (calls.GetObjectRespResponse, error) {
	return calls.GetObjectRespResponse{Count: body, Name: strconv.Itoa(body)}, nil
}

func (q *query) GetObject(body calls.Obj) (calls.Obj, error) {
	return body, nil
}

func (q *query) GetParent(body calls.Parent) (calls.Parent, error) {
	return body, nil
}

func (q *query) GetEnum(body calls.Enum1) (calls.Enum1, error) {
	return body, nil
}

func (q *query) GetNullable(body calls.Nullable) (calls.Nullable, error) {
	return body, nil
}

func (q *query) GetNullableBodyResponse(body *calls.Obj) (*calls.Obj, error) {
	return body, nil
}

func (m *mutation) PutMap(body map[string]calls.Obj) (map[string]calls.Obj, error) {
	return body, nil
}

func (m *mutation) PutTheDate(body calls.ObjWithDate) error {
	return nil
}

func (m *mutation) PutObjDate(body calls.ObjWithDate) (calls.PutObjDateResponse, error) {
	return calls.PutObjDateResponse{Owd: body}, nil
}

func (m *mutation) PutMapDate(body map[string]calls.ObjWithDate) (map[string]calls.ObjWithDate, error) {
	return body, nil
}

func (m *mutation) PutArrayDate(body []calls.ObjWithDate) ([]calls.ObjWithDate, error) {
	return body, nil
}

func (q *query) Recursive() (calls.Recursive, error) {
	return calls.Recursive{}, nil
}

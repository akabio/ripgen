package main

import (
	"net/http"
	"testing"

	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/body/calls"
	"gitlab.com/akabio/ripgen/test/body/httph"
	"gitlab.com/akabio/ripgen/test/internal/test"
)

func TC(t *testing.T) *test.TC {
	t.Helper()

	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	return test.New(t, router)
}

func TestIntBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/query/getInt", map[string]interface{}{"i": 5})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("5")
}

func TestIntListBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/query/intList", map[string]interface{}{"list": []int{5, 3, 8}})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[5,3,8]")
}

func TestObjectBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/query/getObjectResp", map[string]interface{}{"count": 7})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"count":7,"name":"7"}`)
}

func TestObjectDefBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/query/getObject", map[string]interface{}{"body": &calls.Obj{
		Name:  "Foo",
		Count: 7,
		Enum:  "e1a",
		Data:  3.14,
	}})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"count":7,"name":"Foo","enum":"e1a","data":3.14}`)
}

func TestNullable(t *testing.T) {
	tc := TC(t)
	defer tc.Close()

	str := "foo"
	resp := tc.Post("/query/getNullable", map[string]interface{}{"body": &calls.Nullable{Name: &str}})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"obj":null,"count":null,"name":"foo","enum":null,"strlist":null,"nlstrlist":null}`)
}

func TestEnumBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()

	resp := tc.Post("/query/getEnum", map[string]interface{}{"body": "e1a"})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`"e1a"`)
}

func TestWrongEnumBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()

	resp := tc.Post("/query/getEnum", map[string]interface{}{"body": "e1z"})
	resp.Expect.Status.ToBe(http.StatusBadRequest)
}

func TestMissingBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()

	resp := tc.Post("/query/getEnum", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected a body but none was received")
}

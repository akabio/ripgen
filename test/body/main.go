package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/body/calls"
	"gitlab.com/akabio/ripgen/test/body/domain"
	"gitlab.com/akabio/ripgen/test/body/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return &domain.Domain{}, nil
}

func main() {
	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	log.Fatal(http.ListenAndServe(":8001", router))
}

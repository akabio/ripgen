package domain

import (
	"errors"
	"strconv"
	"time"

	"gitlab.com/akabio/ripgen/test/query/calls"
)

type Domain struct{}

type query struct{}

func (d *Domain) Query() calls.Query {
	return &query{}
}

func (q *query) ItemID(weight int, name string) (int, error) {
	if name == "error" {
		return 0, errors.New("invalid param error")
	}

	return weight + len(name), nil
}

func (q *query) Item(param int) (calls.ItemResponse, error) {
	return calls.ItemResponse{
		Foo: time.Date(2020, 1, 1, 0, param, 0, 0, time.UTC),
		Bar: "bar" + strconv.Itoa(param),
	}, nil
}

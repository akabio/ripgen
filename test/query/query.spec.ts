import { expect } from 'chai'
import { Client } from './ts/client.gen'
import { encoder, decoder } from './ts/mapper.gen'

const api = new Client<string, Date>(
  'http://localhost:8000/query',
  async (url, method, body) => {
    return await fetch(url, { method, body })
  },
  encoder({
    id: (d) => d,
    time: (d) => d.toISOString(),
  }),
  decoder({
    id: (s) => s,
    time: (s) => new Date(s),
  }),
)

describe('queries', function () {
  describe('fetch', function () {
    it('is number', async function () {
      expect(await api.fetch("145")).to.eql({"bar":"bar145", "foo": new Date("2020-01-01T02:25:00.000Z")})
    })
  })
  describe('', function () {
    it("it's the same time", async function () {
      expect(
        await api.getIt(100, "zebs")
      ).to.eql("104")
    })
  })
})

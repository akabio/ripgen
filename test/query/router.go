package query

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/query/calls"
	"gitlab.com/akabio/ripgen/test/query/decorator"
	"gitlab.com/akabio/ripgen/test/query/domain"
	"gitlab.com/akabio/ripgen/test/query/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return decorator.New(func(next func() error) error {
		return next()
	}, &domain.Domain{}), nil
}

func CreateRouter() http.Handler {
	return httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
}

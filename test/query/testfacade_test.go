package query

import (
	"testing"

	"github.com/akabio/expect"
)

func TestQueryExpect(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectItemID(1400, "foo").ToBe(1403)
}

func TestExpectQueryErrorNone(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectItemIDError(1400, "foo").ToBe(nil)
}

func TestExpectQueryErrorHas(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectItemIDError(1400, "error").Message().
		ToBe("internal server error, invalid param error")
}

func TestQueryMust(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	resp := tc.Query().MustItemID(1500, "fooo")
	expect.Value(t, "response", resp).ToBe(1504)
}

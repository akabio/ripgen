package query

import (
	"testing"
	"time"

	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/internal/test"
	"gitlab.com/akabio/ripgen/test/query/calls"
	"gitlab.com/akabio/ripgen/test/query/client"
	"gitlab.com/akabio/ripgen/test/query/httph"
	"gitlab.com/akabio/ripgen/test/query/testf"
)

func TC(t *testing.T) *test.TC {
	t.Helper()

	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	return test.New(t, router)
}

type rt struct {
	tc *test.TC
	*testf.Facade
}

func tf(t *testing.T) *rt {
	t.Helper()

	tc := test.New(t, CreateRouter())

	return &rt{
		tc:     tc,
		Facade: testf.New(t, client.Create(tc.C())),
	}
}

func (r *rt) Close() {
	r.tc.Close()
}

func TestFetchUsingClient(t *testing.T) {
	tc := tf(t)
	defer tc.Close()

	tc.Query().ExpectItem(123).ToBe(calls.ItemResponse{
		Foo: time.Date(2020, 1, 1, 2, 3, 0, 0, time.UTC),
		Bar: "bar123",
	})
}

package main

import (
	"testing"

	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/example/httph"
	"gitlab.com/akabio/ripgen/test/internal/test"
)

func TestExample(t *testing.T) {
	tc := test.New(t, httph.CreateRouter(callsF, ripgen.DefaultErrorHandler))
	defer tc.Close()
	resp := tc.Post("/query/getFooById", map[string]interface{}{
		"id":    5,
		"count": 10,
		"order": "bla",
	})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[1,2,3]")
}

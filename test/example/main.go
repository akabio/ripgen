package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/runtime"
	"gitlab.com/akabio/ripgen/test/example/calls"
	"gitlab.com/akabio/ripgen/test/example/decorator"
	"gitlab.com/akabio/ripgen/test/example/domain"
	"gitlab.com/akabio/ripgen/test/example/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	prof := &domain.Domain{}

	return decorator.NewWithContext(func(ctx *runtime.DecoratorContext, next func() error) error {
		return next()
	}, prof), nil
}

func main() {
	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	log.Fatal(http.ListenAndServe(":8001", router))
}

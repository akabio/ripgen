package domain

import (
	"fmt"
	"io"

	"gitlab.com/akabio/ripgen/test/example/calls"
)

type Domain struct{}

type mutation struct{}

type query struct{}

type stream struct{}

func (d *Domain) Mutation() calls.Mutation {
	return &mutation{}
}

func (d *Domain) Query() calls.Query {
	return &query{}
}

func (d *Domain) Stream() calls.Stream {
	return &stream{}
}

func (m *mutation) AddString(str string) error {
	return nil
}

func (m *mutation) UpdateDesc(id int, desc calls.Desc) (int, error) {
	return 0, nil
}

func (q *query) Strings() ([]string, error) {
	return []string{"foo", "bar"}, nil
}

func (m *mutation) CreateFoo(bar string) error {
	fmt.Println("create foo")

	return nil
}

func (q *query) GetFooByID(id int, count int, order string) ([]int, error) {
	fmt.Printf("get foo by id %v\n", id)
	return []int{1, 2, 3}, nil
}

func (m *mutation) Trigger() error {
	return nil
}

func (q *query) Global() (int, error) {
	return 2, nil
}

func (q *query) MapPointers() (calls.MapPointersResponse, error) {
	return calls.MapPointersResponse{}, nil
}

func (s *stream) Render(a calls.FooBar, o *calls.Desc, w io.Writer) error {
	for c := 0; c < 100000; c++ {
		_, err := w.Write([]byte{1, 2, 3, 4})
		if err != nil {
			return err
		}
	}

	return nil
}

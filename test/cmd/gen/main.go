package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"time"

	"gitlab.com/akabio/ripgen"
)

func main() {
	_, filename, _, _ := runtime.Caller(0)
	rips := path.Dir(path.Dir(path.Dir(filename)))

	files, err := os.ReadDir(rips)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			dataSrc, err := os.ReadFile(filepath.Join(rips, f.Name(), f.Name()+".rip"))
			if err != nil {
				if os.IsNotExist(err) {
					continue
				}

				log.Fatal(err)
			}

			fmt.Println("generate", f.Name(), "...")

			st := time.Now()

			data := makeSrc(dataSrc, rips, f.Name())

			gen, err := ripgen.NewGenerator(data, f.Name())
			if err != nil {
				log.Fatal(err)
			}

			err = generateGo(gen, rips, f.Name())
			if err != nil {
				log.Fatal(err)
			}

			err = generateTS(gen, rips, f.Name())
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("total time", time.Since(st))
		}
	}

	fmt.Println("generated test handlers")
}

func makeSrc(src []byte, root, name string) []ripgen.Source {
	data := []ripgen.Source{
		{
			Source: src,
			Name:   name + ".rip",
		},
	}

	if name == "example" {
		dataSrc, err := os.ReadFile(filepath.Join(root, "example/other.rip"))
		if err != nil {
			log.Fatal(err)
		}

		data = append(data, ripgen.Source{
			Source: dataSrc,
			Name:   "other.rip",
		})
	}

	return data
}

func generateTS(gg *ripgen.Generator, root, name string) error {
	externals := map[string]string{
		"Date": "Date",
		"Id":   "number",
		"Time": "Date",
	}
	tsParams := ripgen.TSParams{
		Externals:            externals,
		SingleParamMutations: true,
	}

	err := gg.TypescriptTypes(filepath.Join(root, name, "ts", "types.gen.d.ts"), tsParams)
	if err != nil {
		return err
	}

	err = gg.TypescriptClient(filepath.Join(root, name, "ts", "client.gen.ts"), tsParams)
	if err != nil {
		return err
	}

	err = gg.TypescriptMapper(filepath.Join(root, name, "ts", "mapper.gen.ts"), tsParams)
	if err != nil {
		return err
	}

	err = gg.TanstackVueQuery(filepath.Join(root, name, "ts", "vquery.gen.ts"), tsParams)
	if err != nil {
		return err
	}

	dbg, err := gg.Debug()
	if err != nil {
		return err
	}

	err = os.WriteFile(filepath.Join(root, name, name+".gen.txt"), dbg, 0o600)
	if err != nil {
		return err
	}

	return nil
}

func generateGo(gen *ripgen.Generator, root, name string) error {
	goRootPkg := filepath.Join(root, name)
	rootImport := path.Join("gitlab.com/akabio/ripgen/test", name)

	gg := gen.Go(
		path.Join(rootImport, "calls"),
		"gitlab.com/akabio/ripgen/test/externals",
		map[string]string{
			"Date": "time.Time",
			"Id":   "int",
			"Time": "time.Time",
		},
	)

	err := gg.Interface(goRootPkg + "/calls")
	if err != nil {
		return err
	}

	err = gg.Server(goRootPkg + "/httph")
	if err != nil {
		return err
	}

	err = gg.Decorator(goRootPkg + "/decorator")
	if err != nil {
		return err
	}

	err = gg.Client(goRootPkg + "/client")
	if err != nil {
		return err
	}

	err = gg.TestFacade(goRootPkg + "/testf")
	if err != nil {
		return err
	}

	return nil
}

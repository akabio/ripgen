package domain

import (
	"errors"
	"fmt"

	"gitlab.com/akabio/ripgen/test/mutation/calls"
)

type Domain struct{}

type mutation struct{}

func (d *Domain) Mutation() calls.Mutation {
	return &mutation{}
}

func (m *mutation) SendThing(weight int, name string) (int, error) {
	if name == "error" {
		return 0, errors.New("invalid param error")
	}

	return weight + len(name), nil
}

func (m *mutation) AbortThing(_ int) error {
	return nil
}

func (m *mutation) AnyType(a interface{}) (interface{}, error) {
	fmt.Println("receives", a)
	return a, nil
}

func (m *mutation) MapType(a map[string]int) (map[string]int, error) {
	fmt.Println("receives", a)
	return a, nil
}

func (m *mutation) MapMapType(a map[string]map[string]int) (map[string]map[string]int, error) {
	fmt.Println("receives", a)
	return a, nil
}

func (m *mutation) DoIt() error {
	return nil
}

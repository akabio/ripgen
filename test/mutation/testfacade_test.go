package mutation

import (
	"testing"

	"github.com/akabio/expect"
)

func TestMutationExpect(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Mutation().ExpectSendThing(1400, "foo").ToBe(1403)
}

func TestExpectMutationErrorNone(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Mutation().ExpectSendThingError(1400, "foo").ToBe(nil)
}

func TestExpectMutationErrorHas(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Mutation().ExpectSendThingError(1400, "error").Message().
		ToBe("internal server error, invalid param error")
}

func TestMutationMust(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	resp := tc.Mutation().MustSendThing(1500, "fooo")
	expect.Value(t, "response", resp).ToBe(1504)
}

import { expect } from 'chai'
import { Client } from './ts/client.gen'
import { encoder, decoder } from './ts/mapper.gen'

const api = new Client<string>(
  'http://localhost:8000/mutation',
  async (url, method, body) => {
    return await fetch(url, { method, body })
  },
  encoder({
    id: (d) => d,
  }),
  decoder({
    id: (s) => s,
  }),
)

describe('mutations', function () {
  describe('abortThing', function () {
    it('is number', async function () {
      expect(await api.abortThing("145")).to.eql(undefined);
    })
  })
  describe('send thing', function () {
    it("it's the same time", async function () {
      expect(
        await api.sendThing({weight: 100, name: "zebs"})
      ).to.eql("104");
    })
  })
})

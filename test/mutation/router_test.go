package mutation

import (
	"testing"

	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/internal/test"
	"gitlab.com/akabio/ripgen/test/mutation/client"
	"gitlab.com/akabio/ripgen/test/mutation/httph"
	"gitlab.com/akabio/ripgen/test/mutation/testf"
)

func TC(t *testing.T) *test.TC {
	t.Helper()

	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	return test.New(t, router)
}

type rt struct {
	tc *test.TC
	*testf.Facade
}

func tf(t *testing.T) *rt {
	t.Helper()

	tc := test.New(t, CreateRouter())

	return &rt{
		tc:     tc,
		Facade: testf.New(t, client.Create(tc.C())),
	}
}

func (r *rt) Close() {
	r.tc.Close()
}

func TestSendThing(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/mutation/sendThing", map[string]interface{}{
		"weight": 1000,
		"name":   "Foo",
	})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`"1003"`)
}

func TestAbortThing(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Post("/mutation/abortThing", map[string]interface{}{"id": "5"})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("")
}

func TestAbortThingUsingClient(t *testing.T) {
	tf := tf(t)
	defer tf.Close()

	tf.Mutation().MustAbortThing(123)
}

func TestMutationAnyType(t *testing.T) {
	tf := tf(t)
	defer tf.Close()

	tf.Mutation().ExpectAnyType("hello").ToBe("hello")
	tf.Mutation().ExpectAnyType(7).ToBe(7.0)
	tf.Mutation().ExpectAnyType(map[string]string{"foo": "bar"}).ToBe(map[string]interface{}{"foo": "bar"})
}

func TestMutationMapType(t *testing.T) {
	tf := tf(t)
	defer tf.Close()

	tf.Mutation().ExpectMapType(map[string]int{"foo": 1, "bar": 2}).ToBe(map[string]int{"foo": 1, "bar": 2})
}

package mutation

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/mutation/calls"
	"gitlab.com/akabio/ripgen/test/mutation/decorator"
	"gitlab.com/akabio/ripgen/test/mutation/domain"
	"gitlab.com/akabio/ripgen/test/mutation/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return decorator.New(func(next func() error) error {
		return next()
	}, &domain.Domain{}), nil
}

func CreateRouter() http.Handler {
	return httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
}

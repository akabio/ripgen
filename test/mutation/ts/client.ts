import { Client } from "./client.gen";
import { decoder, encoder } from "./mapper.gen";

const fetcher = async (
  url: string,
  method: string,
  body: undefined | string
) => {
  return new Response();
};

export const client = new Client(
  "/api/admin",
  fetcher,
  encoder({
    id: (d: number) => `${d}`,
  }),

  decoder({
    id: (d: string) => parseInt(d, 10),
  })
);

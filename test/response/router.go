package response

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/response/calls"
	"gitlab.com/akabio/ripgen/test/response/decorator"
	"gitlab.com/akabio/ripgen/test/response/domain"
	"gitlab.com/akabio/ripgen/test/response/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return decorator.New(func(next func() error) error {
		return next()
	}, &domain.Domain{}), nil
}

func CreateRouter() http.Handler {
	return httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
}

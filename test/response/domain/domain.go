package domain

import (
	"gitlab.com/akabio/ripgen/test/response/calls"
)

type Domain struct{}

type query struct{}

func (d *Domain) Query() calls.Query {
	return &query{}
}

func (q *query) ReturnInt(pathInt int) (int, error) {
	return pathInt, nil
}

func (q *query) ReturnID(pathID int) (int, error) {
	return pathID, nil
}

func (q *query) ReturnFloat(pathInt float64) (float64, error) {
	return pathInt, nil
}

func (q *query) ReturnIntList() ([]int, error) {
	return []int{0, 5, 10, -1000}, nil
}

func (q *query) ReturnString(pathStr string) (string, error) {
	return pathStr, nil
}

func (q *query) ReturnStringList() ([]string, error) {
	return []string{"zab", "foo", "b", "a", `"`, "?"}, nil
}

func (q *query) Bool() (bool, error) {
	return true, nil
}

func (q *query) BoolList() ([]bool, error) {
	return []bool{true, false, true}, nil
}

func (q *query) GetEnum(pathEnum calls.Enum2) (calls.Enum2, error) {
	return pathEnum, nil
}

func (q *query) GetEnumList() ([]calls.Enum2, error) {
	return []calls.Enum2{calls.Enum2E1a, calls.Enum2E1c, calls.Enum2E1a}, nil
}

func (q *query) GetObject() (calls.GetObjectResponse, error) {
	return calls.GetObjectResponse{
		Name:  "foo",
		Count: 12,
		T:     calls.Enum2E1a,
	}, nil
}

func (q *query) GetObjectList() ([]calls.RespObj, error) {
	return []calls.RespObj{
		{
			Name:  "foo",
			Count: 12,
			T:     calls.Enum2E1a,
		},
		{
			Name:  "bar",
			Count: -10,
			T:     calls.Enum2E1c,
		},
	}, nil
}

func (q *query) TheMap() (map[string]string, error) {
	return map[string]string{"a": "b"}, nil
}

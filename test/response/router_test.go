package response

import (
	"testing"

	"gitlab.com/akabio/ripgen/test/internal/test"
	"gitlab.com/akabio/ripgen/test/response/calls"
	"gitlab.com/akabio/ripgen/test/response/client"
	"gitlab.com/akabio/ripgen/test/response/testf"
)

type rt struct {
	tc *test.TC
	*testf.Facade
}

func tf(t *testing.T) *rt {
	t.Helper()

	tc := test.New(t, CreateRouter())

	return &rt{
		tc:     tc,
		Facade: testf.New(t, client.Create(tc.C())),
	}
}

func (r *rt) Close() {
	r.tc.Close()
}

func TestIntResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectReturnInt(5).ToBe(5)
}

func TestIntListResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectReturnIntList().ToBe([]int{0, 5, 10, -1000})
}

func TestStringResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectReturnString("foo bar").ToBe("foo bar")
}

func TestStringListResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectReturnStringList().ToBe([]string{"zab", "foo", "b", "a", "\"", "?"})
}

func TestEnumResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectGetEnum("e1a").ToBe(calls.Enum2E1a)
}

func TestEnumListResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectGetEnumList().ToBe([]calls.Enum2{"e1a", "e1c", "e1a"})
}

func TestObjectResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectGetObject().ToBe(calls.GetObjectResponse{Name: "foo", Count: 12, T: "e1a"})
}

func TestObjectListResponse(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Query().ExpectGetObjectList().ToBe([]calls.RespObj{
		{Name: "foo", Count: 12, T: "e1a"},
		{Name: "bar", Count: -10, T: "e1c"},
	})
}

import { expect } from 'chai'
import { Client } from './ts/client.gen'
import { encoder, decoder } from './ts/mapper.gen'

const api = new Client<string>(
  'http://localhost:8000/response',
  async (url, method, body) => {
    return await fetch(url, { method, body })
  },
  encoder({
    id: (s) => s,
  }),
  decoder({
    id: (s) => s,
  }),
)

describe('response', function () {
  describe('using int', function () {
    it('is number', async function () {
      expect(await api.returnInt(7)).to.be.equal(7)
    })
    it('is negative number', async function () {
      expect(await api.returnInt(-123)).to.be.equal(-123)
    })
  })
  describe('using float', function () {
    it('is number', async function () {
      expect(await api.returnFloat(7)).to.be.equal(7)
    })
    it('is negative number', async function () {
      expect(await api.returnFloat(-123)).to.be.equal(-123)
    })
    it('has decimal part', async function () {
      expect(await api.returnFloat(24.539)).to.be.equal(24.539)
    })
  })
})

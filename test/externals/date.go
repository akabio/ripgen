package externals

import (
	"time"
)

func ParseDate(t string) (time.Time, error) {
	return time.Parse("2006-01-02", t)
}

func SerializeDate(d time.Time) (string, error) {
	return d.Format("2006-01-02"), nil
}

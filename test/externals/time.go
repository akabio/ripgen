package externals

import (
	"time"
)

func ParseTime(t string) (time.Time, error) {
	return time.Parse(time.RFC3339Nano, t)
}

func SerializeTime(t time.Time) (string, error) {
	return t.Format(time.RFC3339Nano), nil
}

package externals

import (
	"strconv"
)

func ParseID(id string) (int, error) {
	return strconv.Atoi(id)
}

func SerializeID(id int) (string, error) {
	return strconv.Itoa(id), nil
}

func ZeroID() int {
	return 0
}

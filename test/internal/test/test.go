package test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/akabio/expect"
)

type TC struct {
	t *testing.T
	s *httptest.Server
	c *http.Client
}

func New(t *testing.T, handler http.Handler) *TC {
	t.Helper()

	tc := &TC{
		t: t,
		s: httptest.NewServer(handler),
		c: &http.Client{},
	}

	return tc
}

func (tc *TC) Close() {
	tc.s.Close()
}

func (tc *TC) Get(url string, body interface{}) Response {
	return tc.do("GET", url, body)
}

func (tc *TC) Post(url string, body interface{}) Response {
	return tc.do("POST", url, body)
}

func (tc *TC) C() (string, *http.Client, func(method, url string, body io.Reader) (*http.Request, error)) {
	return tc.URL(), tc.Client(), tc.Factory()
}

func (tc *TC) URL() string {
	return tc.s.URL
}

func (tc *TC) Client() *http.Client {
	return tc.c
}

func (tc *TC) Factory() func(method, url string, body io.Reader) (*http.Request, error) {
	return func(method, url string, body io.Reader) (*http.Request, error) {
		req, err := http.NewRequest(method, url, body)
		if err != nil {
			tc.t.Fatal(err)
		}

		return req, nil
	}
}

func (tc *TC) do(method, url string, body interface{}) Response {
	var bodyBuffer *bytes.Buffer
	var bodyBufferReader io.Reader

	if body != nil {
		jsonBytes, err := json.Marshal(body)
		if err != nil {
			tc.t.Fatal(err)
		}

		bodyBuffer = bytes.NewBuffer(jsonBytes)
		bodyBufferReader = bodyBuffer
	}

	req, err := http.NewRequest(method, tc.s.URL+url, bodyBufferReader)
	if err != nil {
		tc.t.Fatal(err)
	}

	resp, err := tc.c.Do(req)
	if err != nil {
		tc.t.Fatal(err)
	}
	defer resp.Body.Close()

	d, err := io.ReadAll(resp.Body)
	if err != nil {
		d = []byte{}
	}

	return Response{
		Expect: ExpectResponse{
			Status:  expect.Value(tc.t, "status", resp.StatusCode),
			BodyStr: expect.Value(tc.t, "body as string", string(d)),
		},
	}
}

type Response struct {
	Expect ExpectResponse
}

type ExpectResponse struct {
	Status  expect.Val
	BodyStr expect.Val
}

package stream

import (
	"bytes"
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/ripgen/test/stream/domain"
)

func TestExpect(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Stream().ExpectExport("foo").ToBe(domain.Data1k)
}

func TestExpectError(t *testing.T) {
	tc := tf(t)
	defer tc.Close()
	tc.Stream().ExpectExportError("error").Message().
		ToBe("internal server error, oups, shite")
}

func TestMust(t *testing.T) {
	tc := tf(t)
	defer tc.Close()

	buf := bytes.NewBuffer(nil)
	tc.Stream().MustExport("foo", buf)
	expect.Value(t, "export response", buf.Bytes()).ToBe(domain.Data1k)
}

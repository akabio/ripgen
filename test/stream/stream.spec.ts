import { expect } from 'chai'
import { Client } from './ts/client.gen'
import { encoder, decoder } from './ts/mapper.gen'

const api = new Client(
  'http://localhost:8000/stream',
  async (url, method, body) => {
    return await fetch(url, { method, body })
  },
  encoder(),
  decoder(),
)

describe('mutations', function () {
  describe('fetch', function () {
    it('byte stream', async function () {
      const cb = (d: Uint8Array) => {
        expect(d.length).eql(1000);
      }
      await api.export("foo", cb);
    })
  })
})

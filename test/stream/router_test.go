package stream

import (
	"testing"

	"gitlab.com/akabio/ripgen"
	"gitlab.com/akabio/ripgen/test/internal/test"
	"gitlab.com/akabio/ripgen/test/stream/client"
	"gitlab.com/akabio/ripgen/test/stream/httph"
	"gitlab.com/akabio/ripgen/test/stream/testf"
)

func TC(t *testing.T) *test.TC {
	t.Helper()

	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	return test.New(t, router)
}

type rt struct {
	tc *test.TC
	*testf.Facade
}

func tf(t *testing.T) *rt {
	t.Helper()

	tc := test.New(t, CreateRouter())

	return &rt{
		tc:     tc,
		Facade: testf.New(t, client.Create(tc.C())),
	}
}

func (r *rt) Close() {
	r.tc.Close()
}

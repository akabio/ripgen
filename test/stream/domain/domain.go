package domain

import (
	"errors"
	"io"

	"gitlab.com/akabio/ripgen/test/stream/calls"
)

var Data1k = func() []byte {
	d := []byte{}
	for i := 0; i < 100; i++ {
		for j := 0; j < 10; j++ {
			d = append(d, byte(i))
		}
	}
	return d
}()

type Domain struct{}

type stream struct{}

func (d *Domain) Stream() calls.Stream {
	return &stream{}
}

func (q *stream) Export(format string, w io.Writer) error {
	if format == "error" {
		return errors.New("oups, shite")
	}

	_, err := w.Write(Data1k)
	if err != nil {
		return err
	}

	return nil
}

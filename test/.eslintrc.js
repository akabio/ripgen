module.exports = {
  env: {
    es2020: true,
    node: true,
    mocha: true
  },
  extends: [
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint', 'mocha'
  ],
  rules: {
    // 'no-unused-vars': 0,
    // '@typescript-eslint/no-unused-vars': 1,
    // 'comma-dangle': 0,
    // indent: 0,
    // 'space-in-parens': 0,
    // 'no-trailing-spaces': 0,
    // 'no-multiple-empty-lines': 0,
    // 'eol-last': 0,
    // 'padded-blocks': 0
  }
}
